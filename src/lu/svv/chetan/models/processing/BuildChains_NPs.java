package lu.svv.chetan.models.processing;

import java.util.List;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.stanford.DependencyRelation;
import lu.svv.chetan.models.data.Concept_Class;
import lu.svv.chetan.models.utils.Utilities;

public class BuildChains_NPs {
	
	static Document doc;
	
	public static void buildChain(Document annotatedDoc)
	{
		AnnotationSet inputAS = annotatedDoc.getAnnotations();
		AnnotationSet NPs = inputAS.get("Parse_NP");
		for(Annotation NP: NPs)
		{
			if(NP.getFeatures().get("dependencies")!= null)
			{
			@SuppressWarnings("unchecked")
			List<DependencyRelation> list_dependencies = (List<DependencyRelation>) NP.getFeatures().get("dependencies");
			for(DependencyRelation rel: list_dependencies)
			{
				if(rel.getType().contains("prep"))
				{
					FeatureMap features = Factory.newFeatureMap();
					Concept_Class NP1 = Utilities.getMapped_NPPrunedString(annotatedDoc, NP.getId());					

					String relation = rel.getType().replaceAll("prep(c)?_", "");
					Concept_Class target_cl;
					String target;
					if(rel.getType().contains("prepc"))
					{
						target_cl = Utilities.getNextNPinSentence(annotatedDoc, rel.getTargetId());
						target = target_cl.getName();
					}
					else
					{
						target_cl = Utilities.getMapped_NPPrunedString(annotatedDoc, rel.getTargetId());
						target = target_cl.getName();
					}
					
					features.put("source_ID", NP1.getID());
					features.put("source_Type", "Parse_NP");
					features.put("source_String", NP1.getName());

					features.put("target_ID", target_cl.getID());
					features.put("target_Type", "Parse_NP");
					features.put("target_String", target);
					features.put("relation_Type", rel.getType());
					features.put("kind", "NP_NP");
					
					features.put("cardinality", NP1.getCardinality());
					//Add annotation
					//if(!((relation.equals(""))))
					{
						Utilities.addAnnotation(annotatedDoc, NP, inputAS.get(target_cl.getID()), features, "Chain_1");
					}					
				}
				}			
			}
		}
	}
}
