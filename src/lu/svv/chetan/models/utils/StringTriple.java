package lu.svv.chetan.models.utils;

public class StringTriple {
    private String a, b, c;

    public StringTriple (String a, String b, String c) {
      this.a = a;
      this.b = b;
      this.c = c;
    }

    public String getA() {
      return a;
    }

    public String getB() {
      return b;
    }

    public String getC() {
      return c;
    }

    public void setA(String a) {
      this.a = a;
    }

    public void setB(String b) {
      this.b = b;
    }

    public void setC(String c) {
      this.c = c;
    }

    public String toString() {
      return "(" + a.toString() + "," + b.toString() + "," + c.toString() + ")";
    }

    public boolean equals(Object anObject) {
        if (this == anObject) {
            return true;
        }

        if (anObject instanceof StringTriple) {
            StringTriple triple = (StringTriple) anObject;
            if (this.getA().equals(triple.getA()) && this.getB().equals(triple.getB()) && this.getC().equals(triple.getC())) {
                return true;
            }
        }

        return false;
    }

}
