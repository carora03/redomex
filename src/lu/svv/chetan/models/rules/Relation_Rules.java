package lu.svv.chetan.models.rules;

import java.util.HashMap;
import java.util.List;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Utils;
import gate.stanford.DependencyRelation;
import gate.util.InvalidOffsetException;
import lu.svv.chetan.models.data.Concept_Class;
import lu.svv.chetan.models.data.GlobalVariables;
import lu.svv.chetan.models.utils.Utilities;

public class Relation_Rules {
	
	static HashMap<Integer, String> VPsMap = new HashMap<Integer, String>();
	static boolean isAdvMod = false;
	
	/*
	 * Tasks - Prepare VP_NP Chains which connect VPs to NPs by prepositions
	 */
	public static void extractRelations() throws InvalidOffsetException
	{
		Document annotatedDoc = GlobalVariables.annotated_doc;
		AnnotationSet VPs = annotatedDoc.getAnnotations().get("Relation_Verb");
				
		for(Annotation VP: VPs)
		{
			int num_Objects = 0;
			isAdvMod = false;
			String relVerb = Utils.cleanStringFor(annotatedDoc, VP);
			String VPStr = getVPString(annotatedDoc, VP);
			
			
			FeatureMap VP_features = VP.getFeatures();
			FeatureMap updatedFeatures = Factory.newFeatureMap();
			@SuppressWarnings("unchecked")
			List<DependencyRelation> list_dependencies = (List<DependencyRelation>) VP_features.get("dependencies");
			
			for(DependencyRelation rel: list_dependencies)
			{
				System.out.println(rel.getType() + " -- " + rel.getTargetId());
				if(rel.getType().equals("nsubj") || rel.getType().equals("xsubj"))
				{
					int Subject_Id = Utilities.getMapped_NP(annotatedDoc, rel.getTargetId());
					updatedFeatures.put("Subject", Subject_Id);
				}
				else if(rel.getType().equals("xsubj"))
				{
					int Subject_Id = Utilities.getMapped_NP(annotatedDoc, rel.getTargetId());
					updatedFeatures.put("xSubject", Subject_Id);
				}
				else if(rel.getType().equals("dobj"))
				{					
					int Subject_Id = Utilities.getMapped_NP(annotatedDoc, rel.getTargetId());
					if(Subject_Id != 0)
					{
						num_Objects++;
						updatedFeatures.put("D_Object_" + num_Objects, Subject_Id);
					}					
				}
				else if(rel.getType().equals("iobj"))
				{
					int Subject_Id = Utilities.getMapped_NP(annotatedDoc, rel.getTargetId());
					updatedFeatures.put("I_Object", Subject_Id);
				}
				else if(rel.getType().equals("pobj"))
				{
					int Subject_Id = Utilities.getMapped_NP(annotatedDoc, rel.getTargetId());
					updatedFeatures.put("P_Object", Subject_Id);
				}				
				else if(rel.getType().equals("nsubjpass"))
				{
					int Subject_Id = Utilities.getMapped_NP(annotatedDoc, rel.getTargetId());
					updatedFeatures.put("Passive_Subject", Subject_Id);
				}
				else if(rel.getType().equals("agent"))
				{
					int Subject_Id = Utilities.getMapped_NP(annotatedDoc, rel.getTargetId());
					updatedFeatures.put("Agent", Subject_Id);
				}
				else if(rel.getType().contains("prep"))
				{
					updatedFeatures.put(rel.getType(), rel.getTargetId());					
					makeVP_PPChains(annotatedDoc, VP, rel);
				}
				else if(isAdvMod)
				{
					updatedFeatures.put("isAdvMod", 1);										
				}
				else if(rel.getType().equals("xcomp")) //Forward Dependency XCOMP is always a verb to verb dependency -- Merge the two verbs as a single relation
				{
					int verb_Id = Utilities.getMapped_VP(annotatedDoc, rel.getTargetId());
					updatedFeatures.put("FD_xcomp", verb_Id);
				}
				else if(rel.getType().equals("ccomp")) //Forward Dependency CCOMP is always a verb to verb dependency -- Merge the two verbs as a single relation
				{
					int verb_Id = Utilities.getMapped_VP(annotatedDoc, rel.getTargetId());
					updatedFeatures.put("FD_ccomp", verb_Id);
				}
			}
			
			updatedFeatures.put("Num_Objects" , num_Objects);
			updatedFeatures.put("str" , VPStr);
			updatedFeatures.put("root", VPStr.replace(relVerb, VP_features.get("root").toString())); //Updated this to add adverb to the root
			annotatedDoc.getAnnotations().add(Utils.start(VP), Utils.end(VP), "Relations", updatedFeatures);			
		}
	}
	
	private static void makeVP_PPChains(Document annotatedDoc, Annotation VP, DependencyRelation rel)
	{
		AnnotationSet inputAS = annotatedDoc.getAnnotations();
		
		FeatureMap features = Factory.newFeatureMap();
		String VP1 = VP.getFeatures().get("string").toString();		

		String relation = rel.getType().replaceAll("prep(c)?_", "");
		Concept_Class target_cl;
		String target;
		if(rel.getType().contains("prepc"))
		{
			target_cl = Utilities.getNextNPinSentence(annotatedDoc, rel.getTargetId());
			target = target_cl.getName();
		}
		else
		{
			target_cl = Utilities.getMapped_NPPrunedString(annotatedDoc, rel.getTargetId());
			target = target_cl.getName();
		}
		
		
		
		features.put("string", VP1 + " " + relation + " " + target);
		features.put("source_ID", VP.getId());
		features.put("source_Type", "VP");
		features.put("source_String", VP1);

		features.put("target_ID", target_cl.getID());
		features.put("target_Type", "Parse_NP");
		features.put("target_String", target);
		features.put("relation_Type", rel.getType());					
		features.put("kind", "VP_NP");
		features.put("cardinality", target_cl.getCardinality());
		if(!(VP1.equals("provide")))
		{
			Utilities.addAnnotation(annotatedDoc, VP, inputAS.get(target_cl.getID()), features, "Chain_1");
		}
	}

//	
//	public static void extractPrepRelations(Document annotatedDoc) throws InvalidOffsetException
//	{
//		AnnotationSet preps_with = annotatedDoc.getAnnotations().get("Dep-prep_with");
//		
//		for(Annotation prep_with: preps_with)
//		{
//			
//			FeatureMap prep_with_features = prep_with.getFeatures();
//			FeatureMap updatedFeatures = Factory.newFeatureMap();
//		
//			int rel_t1 = (int) prep_with_features.get("token1_Id");
//			int rel_t2 = (int) prep_with_features.get("token2_Id");
//			
//			int Phrase1_Id = Utilities.getMapped_NP(annotatedDoc, rel_t1);
//			if(Phrase1_Id == 0)
//			{
//				Phrase1_Id = Utilities.getMapped_VP(annotatedDoc, rel_t1);
//				updatedFeatures.put("P1_Id", Phrase1_Id);
//				updatedFeatures.put("P1_Type", "VP");
//			}
//			else
//			{
//				updatedFeatures.put("P1_Id", Phrase1_Id);
//				updatedFeatures.put("P1_Type", "NP");
//			}
//			
//			int Phrase2_Id = Utilities.getMapped_NP(annotatedDoc, rel_t2);
//			if(Phrase2_Id == 0)
//			{
//				Phrase2_Id = Utilities.getMapped_VP(annotatedDoc, rel_t2);
//				updatedFeatures.put("P2_Id", Phrase2_Id);
//				updatedFeatures.put("P2_Type", "VP");
//			}
//			else
//			{
//				updatedFeatures.put("P2_Id", Phrase2_Id);
//				updatedFeatures.put("P2_Type", "NP");
//			}						
//			annotatedDoc.getAnnotations().add(Utils.start(prep_with), Utils.end(prep_with), "PP_with", updatedFeatures);			
//		}
//		
//	}

	private static String getVPString(Document doc, Annotation VP)
	{
		AnnotationSet overlappingVP_adv = Utils.getOverlappingAnnotations(doc.getAnnotations(), VP, "Dep-advmod");
		AnnotationSet overlappingVP_PP = Utils.getOverlappingAnnotations(doc.getAnnotations(), VP, "VP_PP");
		String VPStr = Utils.stringFor(doc, VP);
		if(overlappingVP_adv.size() == 1)
		{
			Annotation annot = Utils.getOnlyAnn(overlappingVP_adv);
			VPStr = annot.getFeatures().get("token2").toString().equals(VPStr)?annot.getFeatures().get("token1").toString() + " " + VPStr:VPStr + " " + annot.getFeatures().get("token2").toString();
			isAdvMod = true;
		}
		
		//FIx Mapping of combined ADVMOD AND PP
		if(overlappingVP_PP.size() == 1)
		{
			VPStr = Utils.stringFor(doc, overlappingVP_PP);
		}
		return VPStr;
	}

}
