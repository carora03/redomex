package lu.svv.chetan.models.data;

public enum RelationType {
	ASSOCIATION,
	AGGREGATION,
	GENERALIZATION,
	ATTRIBUTE;
}
