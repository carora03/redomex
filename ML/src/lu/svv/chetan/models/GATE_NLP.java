package lu.svv.chetan.models;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Utils;
import gate.creole.ANNIEConstants;
import gate.creole.ConditionalSerialAnalyserController;
import gate.gui.MainFrame;
import gate.util.persistence.PersistenceManager;

public class GATE_NLP {
	
	//HashMap<String, String> map_ReqIds_
	
	/*
	 * This method initializes all the gate resources and is used to execute the pipeline (defined within this method)
	 * on the document (defined within this method)
	 * It returns the annotated document
	 */
	public static Corpus init() throws Exception
	{
		//Set GATE home to right location 
		File file = new File("/Applications/GATE_Developer_8.2/");
		Gate.setGateHome(file);
		Gate.setSiteConfigFile(new File(file.getPath() + "/gate.xml"));
		
		
		//prepare the GATE library
		Gate.init();
		
		//Show the GATE developer window
		SwingUtilities.invokeAndWait(new Runnable() { public void run() {
			MainFrame.getInstance().setVisible(true); }
		});
		
		//Load Creole Plugins
		Gate.getCreoleRegister().registerDirectories(new File(gate.Gate.getPluginsHome(), ANNIEConstants.PLUGIN_DIR).toURI().toURL());
		Gate.getCreoleRegister().registerDirectories(new File(gate.Gate.getPluginsHome(), "OpenNLP").toURI().toURL());
		Gate.getCreoleRegister().registerDirectories(new File(gate.Gate.getPluginsHome(), "Stanford_CoreNLP").toURI().toURL());
		Gate.getCreoleRegister().registerDirectories(new File(gate.Gate.getPluginsHome(), "Tools").toURI().toURL());
				
		Corpus corpus = (Corpus) Factory.createResource("gate.corpora.CorpusImpl");
		corpus.setName("Test_Corpus");
		
		Document doc_1 = Factory.newDocument(ReadCSV.sj_reqsText.toString());
		//Document doc = (Document) Factory.createResource("gate.corpora.DocumentImpl", params1, featMap, "document");
		doc_1.setName("Requirements DOcument");
				
	    corpus.add(doc_1);
		
		ConditionalSerialAnalyserController pipeline = (ConditionalSerialAnalyserController) PersistenceManager.loadObjectFromFile(new File("/Users/chetan.arora/Dropbox/PhD Folder/Useful_Code/Model Extraction Files/models.gapp"));
		pipeline.setCorpus(corpus);
		pipeline.execute();
		
		return corpus;
	}

	public static void extractData(Corpus corpus) throws Exception
	{
		Document annotatedDoc = corpus.get(0);
		AnnotationSet as_Sentences = annotatedDoc.getAnnotations().get("Sentence");
		List<Annotation> list_sentences = Utils.inDocumentOrder(as_Sentences);
		
		for(int i = 0; i< list_sentences.size(); i++)
		{
			String sentence_text = Utils.stringFor(annotatedDoc, list_sentences.get(i));
			for(int j = i; j < Data.map_reqIDs_Text.keySet().size(); j++)
			{
				String req_Id = (String) Data.map_reqIDs_Text.keySet().toArray()[j];
				String req_Text = Data.map_reqIDs_Text.get(req_Id);
				if(req_Text.equals(sentence_text) || Similarity_Utils.getSimilarityScore(req_Text, sentence_text) > 0.9)
				{
					List<String> list_NPs = new ArrayList<String>();
					List<String> list_VBs = new ArrayList<String>();
					
					/*
					 * Noun Phrases
					 */
					AnnotationSet as_ReqNPs = Utils.getContainedAnnotations(annotatedDoc.getAnnotations(), list_sentences.get(i), "Parse_NP");
					List<Annotation> list_reqNPs = Utils.inDocumentOrder(as_ReqNPs);
					for (Annotation annot_NP : list_reqNPs) {
						FeatureMap NP_featuremap = annot_NP.getFeatures();
						if(NP_featuremap.get("pruned_string").toString().trim().isEmpty())
						{
							list_NPs.add(NP_featuremap.get("root").toString().trim());
						}
						else
						{
							list_NPs.add(NP_featuremap.get("pruned_string").toString().trim());
						}						
					}
					
					/*
					 * Verbs
					 */
					AnnotationSet as_ReqVBs = Utils.getContainedAnnotations(annotatedDoc.getAnnotations(), list_sentences.get(i), "Relation_Verb");
					List<Annotation> list_reqVBs = Utils.inDocumentOrder(as_ReqVBs);
					for (Annotation annot_VB : list_reqVBs) {
						FeatureMap VB_featuremap = annot_VB.getFeatures();						
						list_VBs.add(VB_featuremap.get("root").toString());
					}
					
					Data.map_reqIDs_NPs.put(req_Id, list_NPs);
					Data.map_reqIDs_VBs.put(req_Id, list_VBs);
					break;
				}				
				else
				{
					System.out.println("ERROR:: REQUIREMENT NOT FOUND");
				}
			}
		}

	}
}
