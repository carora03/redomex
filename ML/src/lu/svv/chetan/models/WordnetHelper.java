package lu.svv.chetan.models;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.dictionary.Dictionary;

public class WordnetHelper {

	public static Dictionary wordnet;
	public WordnetHelper(String propsFile)
	{
		initialize(propsFile);
	}
	
	// Initialize the database!
    public static void initialize(String propsFile) {
        //String propsFile = "file_properties.xml";
        try {
            JWNL.initialize(new FileInputStream(propsFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JWNLException e) {
            e.printStackTrace();
        }
        // Create dictionary object
        wordnet = Dictionary.getInstance();
    }
    
    public boolean wordExists(String word) throws JWNLException
	{
		IndexWord[] arrayWords = wordnet.lookupAllIndexWords(word).getIndexWordArray();
		if( arrayWords.length > 0 )
		{
			//System.out.println("rejected word: " + word);
		  return true;
		}
		else
		{
		  return false;
		}
	}
	
}
