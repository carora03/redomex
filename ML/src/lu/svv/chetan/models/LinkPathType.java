package lu.svv.chetan.models;

public enum LinkPathType {
	NAP("NAP"),
	NO("NO"),
	YES("YES"),
	SRC("SRC");
	
	 private final String name;       

	 private LinkPathType(String s) {
	        name = s;
}
	 
	 public boolean equalsName(String otherName) {
	        return (otherName == null) ? false : name.equals(otherName);
	    }

	    public String toString() {
	       return this.name;
	    }
	    
}
