package lu.svv.chetan.models.prepareData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

public class ReadOrderedSet {
	static HashSet<String> set_combos = new HashSet<>();
public static void main(String [] args)
{
	
	for(int i =1; i<=100; i++)
	{
		readOrderedReqIdsFile("./res/ordered_sets/SES/ordered_Relations/SES_VAR" + i + ".txt");
	}
	System.out.println(set_combos.size());
	
}
	
	public static String [] readOrderedReqIdsFile(String fileName) {
		
		  FileReader file = null;
		  BufferedReader reader = null;
		  try {
		    file = new FileReader(fileName);
			reader = new BufferedReader(file);
		    String line = "";
		    int header = 0;
		    while ((line = reader.readLine()) != null) {
		    	set_combos.add(line);
		    	String [] reqIds = line.split(" ");
		    	return reqIds;
		    }
		  } catch (Exception e) {
			  System.out.println("File(s) not Found" + System.lineSeparator());
			  
		      throw new RuntimeException(e);
		  } finally {
		    if (file != null) {
		      try {
		    	  file.close();
		      } catch (IOException e) {
		        // Ignore issues during closing 
		      }
		    }
		  }
		  return new String[0]; 
		}
	
}
