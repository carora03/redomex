package lu.svv.chetan.models.prepareData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import lu.svv.chetan.models.Data;
import lu.svv.chetan.models.Relation;

public class RandomizeOrder {
	
	public static void main(String [] args)
	{
		List<String> set_Ids = new ArrayList<String>();
		set_Ids.add("R1");
		set_Ids.add("R2");
		set_Ids.add("R3");
		set_Ids.add("R4");
		set_Ids.add("R5");
		set_Ids.add("R6");
		set_Ids.add("R7");
		//randomizeOrder(set_Ids);
	}
	
	public static void randomizeOrder_byRelations()
	{		
		List<String> relationIDs = new ArrayList<>(Data.map_AllRelations.keySet());
		Collections.shuffle(relationIDs);
		int total_Relations = Data.map_AllRelations.size();
		int N90_percent_total = (int) (0.1 * total_Relations);
		int curr_size = 0;
		
			
				for(String rel_ID: relationIDs)
				{
					Relation rel = Data.map_AllRelations.get(rel_ID);
					Data.map_TrainingRelations.put(rel_ID, rel);
					curr_size++;
					if(curr_size == N90_percent_total)
						break;
				}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}


	
	public static void randomizeOrder_byReqs()
	{		
		List<String> reqIDs = new ArrayList<>(Data.map_reqIDs_Relations.keySet());
		Collections.shuffle(reqIDs);
		int total_Relations = Data.map_AllRelations.size();
		int N90_percent_total = (int) (0.5 * total_Relations);
		int curr_size = 0;
		
			for(String reqID: reqIDs)
			{
				List<Relation> relations = Data.map_reqIDs_Relations.get(reqID);
				for(Relation rel: relations)
				{
					Data.map_TrainingRelations.put(rel.getRelation_Id(), rel);
					curr_size++;
					if(curr_size == N90_percent_total)
						break;
				}
				if(curr_size == N90_percent_total)
					break;
			}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}
	
	public static void optimalOrder_byRelations(String filename)
	{		
		String [] order = ReadOrderedSet.readOrderedReqIdsFile(filename);
		List<String> relationIDs = Arrays.asList(order); 
		int total_Relations = Data.map_AllRelations.size();
		int N90_percent_total = (int) (0.5 * total_Relations);
		int curr_size = 0;
		
			
				for(String rel_ID: relationIDs)
				{
					Relation rel = Data.map_AllRelations.get(rel_ID);
					Data.map_TrainingRelations.put(rel_ID, rel);
					curr_size++;
					if(curr_size == N90_percent_total)
						break;
				}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}
	
	public static void optimalOrder_byReqs(String filename)
	{
		String [] order = ReadOrderedSet.readOrderedReqIdsFile(filename);
		List<String> reqIDs = Arrays.asList(order); 
		int total_Relations = Data.map_AllRelations.size();
		int N90_percent_total = (int) (0.5 * total_Relations);
		int curr_size = 0;
		
			for(String reqID: reqIDs)
			{
				List<Relation> relations = Data.map_reqIDs_Relations.get(reqID);
				for(Relation rel: relations)
				{
					Data.map_TrainingRelations.put(rel.getRelation_Id(), rel);
					curr_size++;
					if(curr_size == N90_percent_total)
						break;
				}
				if(curr_size == N90_percent_total)
					break;
			}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}
	
	/*
	 * Randomly pick a relation from each requirement
	 */
	public static void diversifiedOrder_byRelations()
	{		
		List<String> reqIDs = new ArrayList<>(Data.map_reqIDs_Relations.keySet());
		
			for(String reqID: reqIDs)
			{
				List<Relation> relations = Data.map_reqIDs_Relations.get(reqID);
				Collections.shuffle(relations);
				Data.map_TrainingRelations.put(relations.get(0).getRelation_Id(), relations.get(0));				
			}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}
	
}
