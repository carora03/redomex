package lu.svv.chetan.models.continuousLearning;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.StringJoiner;


public class WriteData {
	
	public static void writeCSV(LinkedHashMap<String, Relation> map, String filename1, String filename2)
	{
		StringJoiner rel_joiner = new StringJoiner(System.lineSeparator());
		
		//Header
		rel_joiner.add("Req_Id,Rel_Id,Source_Concept,Target_Concept,Relation_Text,Relation_Type,LP-Depth,LP-Type,Rule_Id,Correctness,Relevance,Source_TC,Target_TC,Relation_TC,Rel_Req_Ratio,Source_Conj,Target_Conj,Source_Meaningful,Target_Meaningful,Subset_R,Subset_IR,Superset_R,Superset_IR,CommonEnds_R,CommonEnds_IR,CommonVerbs_R,CommonVerbs_IR,Source_R,Source_IR,Target_R,Target_IR,Relations_SameReq,Relations_SameReq_R,Relations_SameReq_IR,Relations_SameReq_SameRule_R,Relations_SameReq_SameRule_IR");
		for(String rel_ID: map.keySet())
		{
			Relation relation = map.get(rel_ID);
			
			StringJoiner joiner = new StringJoiner(",");			
			joiner.add(relation.getReq_Id());
			joiner.add(relation.getRelation_Id());
			joiner.add(relation.getSource_Concept());
			joiner.add(relation.getTarget_Concept());
			joiner.add(relation.getRelation_Text());
			joiner.add(relation.getType().toString());
			joiner.add("" + relation.getLP_Depth());
			joiner.add("" + relation.getLP_Type());
			joiner.add(relation.getRule_Id());
			joiner.add(relation.getIsCorrect().toString());
			joiner.add(relation.getIsRelevant().toString());
			
			joiner.add("" + relation.getSource_Concept_TC());
			joiner.add("" + relation.getTarget_Concept_TC());
			joiner.add("" + relation.getRelation_Text_TC());
			joiner.add("" + relation.getRel_Req_Ratio());
			joiner.add(("" + relation.getisSourceHavingConjunction()).toUpperCase());
			joiner.add(("" + relation.getisTargetHavingConjunction()).toUpperCase());
			joiner.add(("" + relation.getisSourceSingleMeaningful()).toUpperCase());
			joiner.add(("" + relation.getisTargetSingleMeaningful()).toUpperCase());
			
			//Dependent Factors
			joiner.add("" + relation.getnum_RelevantSubset());
			joiner.add("" + relation.getnum_IrRelevantSubset());
			joiner.add("" + relation.getnum_RelevantSuperset());
			joiner.add("" + relation.getnum_IrRelevantSuperset());
			joiner.add("" + relation.getnum_RelevantSameEndConcepts());
			joiner.add("" + relation.getnum_IrRelevantSameEndConcepts());
			joiner.add("" + relation.getnum_RelevantSameVerbs());
			joiner.add("" + relation.getnum_IrRelevantSameVerbs());
			joiner.add("" + relation.getnum_RelevantSharedSource());
			joiner.add("" + relation.getnum_IrRelevantSharedSource());
			joiner.add("" + relation.getnum_RelevantSharedTarget());
			joiner.add("" + relation.getnum_IrRelevantSharedTarget());
			
			joiner.add("" + relation.getnum_RelationsinSameReq());
			joiner.add("" + relation.getnum_RelevantinSameReq());
			joiner.add("" + relation.getnum_IrRelevantinSameReq());
			joiner.add("" + relation.getnum_RelevantinSameReq_sameRule());
			joiner.add("" + relation.getnum_IrRelevantinSameReq_sameRule());
			
			rel_joiner.add(joiner.toString());
		}
		
		try {
			PrintWriter writer = new PrintWriter(filename1, "UTF-8");
			writer.print(rel_joiner.toString());
			writer.close();
			
			PrintWriter writer1 = new PrintWriter(filename2, "UTF-8");
			writer1.print(rel_joiner.toString());
			writer1.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void writeSimpleData(String data, String filename, boolean append) throws UnsupportedEncodingException
	{
		try {
			PrintWriter writer = new PrintWriter(new FileOutputStream(
				    new File(filename), append));
			writer.print(data);
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
