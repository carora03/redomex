package lu.svv.chetan.models.continuousLearning;

public class Relation {
	
	private String Req_Id;
	private String Relation_Id;
	private String Source_Concept;
	private String Target_Concept;
	private String Relation_Text;
	private RelationType Type;
	private int LP_Depth;
	private LinkPathType LP_Type;
	private String Rule_Id;
	private CorrectnessType IsCorrect;
	private RelevanceType IsRelevant;
	
	private int Source_Concept_TC;
	private int Target_Concept_TC;
	private int Relation_Text_TC;
	
	private boolean isSourceSingleMeaningful;
	private boolean isTargetSingleMeaningful;
	private boolean isSourceHavingConjunction;
	private boolean isTargetHavingConjunction;
	
	//Dependedent Values
	private int num_RelevantSubset = 0;
	private int num_IrRelevantSubset = 0;
	private int num_RelevantSuperset = 0;
	private int num_IrRelevantSuperset = 0;
	private int num_RelevantSameEndConcepts = 0;
	private int num_IrRelevantSameEndConcepts = 0;
	private int num_RelevantSameVerbs = 0;
	private int num_IrRelevantSameVerbs = 0;
	
	private int num_RelevantSharedSource = 0;
	private int num_IrRelevantSharedSource = 0;
	private int num_RelevantSharedTarget = 0;
	private int num_IrRelevantSharedTarget = 0;
	
	private double Rel_Req_Ratio;
	
	private int num_RelationsinSameReq = 0;
	private int num_RelevantinSameReq = 0;
	private int num_IrRelevantinSameReq = 0;
	private int num_RelevantinSameReq_sameRule = 0;
	private int num_IrRelevantinSameReq_sameRule = 0;
	
	private boolean isLPMarked = false;
	
	/*
	 * GETTERS
	 */
	public  String getReq_Id()
	{
		return this.Req_Id;
	}
	public  String getRelation_Id()
	{
		return this.Relation_Id;
	}
	public  String getSource_Concept()
	{
		return this.Source_Concept;
	}
	public  String getTarget_Concept()
	{
		return this.Target_Concept;
	}
	public  String getRelation_Text()
	{
		return this.Relation_Text;
	}
	public  RelationType getType()
	{
		return this.Type;
	}
	public  int getLP_Depth()
	{
		return this.LP_Depth;
	}
	public  LinkPathType getLP_Type()
	{
		return this.LP_Type;
	}
	public  String getRule_Id()
	{
		return this.Rule_Id;
	}
	public  CorrectnessType getIsCorrect()
	{
		return this.IsCorrect;
	}
	public  RelevanceType getIsRelevant()
	{
		return this.IsRelevant;
	}
	
	public  int getSource_Concept_TC()
	{
		return this.Source_Concept_TC;
	}
	public  int getTarget_Concept_TC()
	{
		return this.Target_Concept_TC;
	}
	public  int getRelation_Text_TC()
	{
		return this.Relation_Text_TC;
	}
	public boolean getisSourceSingleMeaningful()
	{
		return this.isSourceSingleMeaningful;
	}
	public boolean getisTargetSingleMeaningful()
	{
		return this.isTargetSingleMeaningful;
	}
	public boolean getisSourceHavingConjunction()
	{
		return this.isSourceHavingConjunction;
	}
	public boolean getisTargetHavingConjunction()
	{
		return this.isTargetHavingConjunction;
	}
	public double getRel_Req_Ratio()
	{
		return this.Rel_Req_Ratio;
	}
	public int getnum_RelevantSubset()
	{
		return this.num_RelevantSubset;
	}
	public int getnum_IrRelevantSubset()
	{
		return this.num_IrRelevantSubset;
	}
	public int getnum_RelevantSuperset()
	{
		return this.num_RelevantSuperset;
	}
	public int getnum_IrRelevantSuperset()
	{
		return this.num_IrRelevantSuperset;
	}
	public int getnum_RelevantSameEndConcepts()
	{
		return this.num_RelevantSameEndConcepts;
	}
	public int getnum_IrRelevantSameEndConcepts()
	{
		return this.num_IrRelevantSameEndConcepts;
	}
	public int getnum_RelevantSameVerbs()
	{
		return this.num_RelevantSameVerbs;
	}
	public int getnum_IrRelevantSameVerbs()
	{
		return this.num_IrRelevantSameVerbs;
	}
	public int getnum_RelevantSharedSource()
	{
		return this.num_RelevantSharedSource;
	}
	public int getnum_IrRelevantSharedSource()
	{
		return this.num_IrRelevantSharedSource;
	}
	public int getnum_RelevantSharedTarget()
	{
		return this.num_RelevantSharedTarget;
	}
	public int getnum_IrRelevantSharedTarget()
	{
		return this.num_IrRelevantSharedTarget;
	}
	public int getnum_RelationsinSameReq()
	{
		return this.num_RelationsinSameReq;
	}
	public int getnum_RelevantinSameReq()
	{
		return this.num_RelevantinSameReq;
	}
	public int getnum_IrRelevantinSameReq()
	{
		return this.num_IrRelevantinSameReq;
	}
	public int getnum_RelevantinSameReq_sameRule()
	{
		return this.num_RelevantinSameReq_sameRule;
	}
	public int getnum_IrRelevantinSameReq_sameRule()
	{
		return this.num_IrRelevantinSameReq_sameRule;
	}
	public boolean getisLPMarked()
	{
		return this.isLPMarked;
	}
	
	
	/*
	 * SETTERS
	 */
	public void setReq_Id(String req_Id)
	{
		this.Req_Id = req_Id;
	}
	public void setRelation_Id(String relation_Id)
	{
		this.Relation_Id = relation_Id;
	}
	
	public  void setSource_Concept(String source_Concept)
	{
		this.Source_Concept = source_Concept;
	}
	public  void setTarget_Concept(String target_Concept)
	{
		this.Target_Concept = target_Concept;
	}
	public  void setRelation_Text(String relation_Text)
	{
		this.Relation_Text = relation_Text; 
	}
	public  void setType(RelationType type)
	{
		this.Type = type;
	}
	public  void setLP_Depth(int lp_Depth)
	{
		this.LP_Depth = lp_Depth;
	}
	public  void setLP_Type(LinkPathType lp_Type)
	{
		this.LP_Type = lp_Type;
	}
	public  void setRule_Id(String rule_Id)
	{
		this.Rule_Id = rule_Id;
	}
	public  void setIsCorrect(CorrectnessType isCorrect)
	{
		this.IsCorrect = isCorrect;
	}
	public  void setIsRelevant(RelevanceType isRelevant)
	{
		this.IsRelevant = isRelevant;
	}
	public  void setSource_Concept_TC(int source_Concept_TC)
	{
		this.Source_Concept_TC = source_Concept_TC;
	}
	public  void setTarget_Concept_TC(int target_Concept_TC)
	{
		this.Target_Concept_TC = target_Concept_TC;
	}
	public void setRelation_Text_TC(int relation_Text_TC)
	{
		this.Relation_Text_TC = relation_Text_TC;
	}
	public  void setisSourceSingleMeaningful(boolean is_sourceSingleMeaningful)
	{
		this.isSourceSingleMeaningful = is_sourceSingleMeaningful;
	}
	public  void setisTargetSingleMeaningful(boolean is_targetSingleMeaningful)
	{
		this.isTargetSingleMeaningful = is_targetSingleMeaningful;
	}
	
	public void setisSourceHavingConjunction(boolean is_sourceHavingConjunction)
	{
		this.isSourceHavingConjunction = is_sourceHavingConjunction;
	}
	public void setisTargetHavingConjunction(boolean is_targetHavingConjunction)
	{
		this.isTargetHavingConjunction = is_targetHavingConjunction;
	}
	public void setRel_Req_Ratio(double rel_Req_Ratio)
	{
		 this.Rel_Req_Ratio = rel_Req_Ratio;
	}
	public void setnum_RelevantSubset(int num_relevantSubset)
	{
		this.num_RelevantSubset = num_relevantSubset;
	}
	public void setnum_IrRelevantSubset(int num_irRelevantSubset)
	{
		this.num_IrRelevantSubset = num_irRelevantSubset;
	}
	public void setnum_RelevantSuperset(int num_relevantSuperset)
	{
		this.num_RelevantSuperset = num_relevantSuperset;
	}
	public void setnum_IrRelevantSuperset(int num_irRelevantSuperset)
	{
		this.num_IrRelevantSuperset = num_irRelevantSuperset;
	}
	public void setnum_RelevantSameEndConcepts(int num_relevantSameEndConcepts)
	{
		this.num_RelevantSameEndConcepts = num_relevantSameEndConcepts;
	}
	public void setnum_IrRelevantSameEndConcepts(int num_irRelevantSameEndConcepts)
	{
		this.num_IrRelevantSameEndConcepts = num_irRelevantSameEndConcepts;
	}
	public void setnum_RelevantSameVerbs(int num_relevantSameVerbs)
	{
		this.num_RelevantSameVerbs = num_relevantSameVerbs;
	}
	public void setnum_IrRelevantSameVerbs(int num_irRelevantSameVerbs)
	{
		this.num_IrRelevantSameVerbs = num_irRelevantSameVerbs;
	}
	public void setnum_RelevantSharedSource(int num_relevantSharedSource)
	{
		this.num_RelevantSharedSource = num_relevantSharedSource;
	}
	public void setnum_IrRelevantSharedSource(int num_irRelevantSharedSource)
	{
		this.num_IrRelevantSharedSource = num_irRelevantSharedSource;
	}
	public void setnum_RelevantSharedTarget(int num_relevantSharedTarget)
	{
		this.num_RelevantSharedTarget = num_relevantSharedTarget;
	}
	public void setnum_IrRelevantSharedTarget(int num_irRelevantSharedTarget)
	{
		this.num_IrRelevantSharedTarget = num_irRelevantSharedTarget;
	}
	public void setnum_RelationsinSameReq(int num_relationsinSameReq)
	{
		this.num_RelationsinSameReq = num_relationsinSameReq;
	}
	public void setnum_RelevantinSameReq(int num_relevantinSameReq)
	{
		this.num_RelevantinSameReq = num_relevantinSameReq;
	}
	public void setnum_IrRelevantinSameReq(int num_irRelevantinSameReq)
	{
		this.num_IrRelevantinSameReq = num_irRelevantinSameReq;
	}
	public void setnum_RelevantinSameReq_sameRule(int num_relevantinSameReq_sameRule)
	{
		this.num_RelevantinSameReq_sameRule = num_relevantinSameReq_sameRule;
	}
	public void setnum_IrRelevantinSameReq_sameRule(int num_irRelevantinSameReq_sameRule)
	{
		this.num_IrRelevantinSameReq_sameRule = num_irRelevantinSameReq_sameRule;
	}
	public void setisLPMarked(boolean isLPMarked)
	{
		this.isLPMarked = isLPMarked;
	}

	
	//RESET the variables related to dependent info
	public void resetDependentInfo()
	{
		this.num_RelevantSubset = 0;
		this.num_IrRelevantSubset = 0;
		this.num_RelevantSuperset = 0;
		this.num_IrRelevantSuperset = 0;
		this.num_RelevantSameEndConcepts = 0;
		this.num_IrRelevantSameEndConcepts = 0;
		this.num_RelevantSameVerbs = 0;
		this.num_IrRelevantSameVerbs = 0;
		
		this.num_RelevantSharedSource = 0;
		this.num_IrRelevantSharedSource = 0;
		this.num_RelevantSharedTarget = 0;
		this.num_IrRelevantSharedTarget = 0;
		
		this.num_RelationsinSameReq = 0;
		this.num_RelevantinSameReq = 0;
		this.num_IrRelevantinSameReq = 0;
		this.num_RelevantinSameReq_sameRule = 0;
		this.num_IrRelevantinSameReq_sameRule = 0;
		
	}
}
