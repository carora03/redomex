package lu.svv.chetan.models.continuousLearning;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class FillDependentRulesData {
	
	
	public static void FillDependentRulesData_TestData()
	{
		for(String rel_id: Data.map_TestRelations.keySet())
		{
			Relation curr_rel = Data.map_TestRelations.get(rel_id);
			DedependentRule_Relevant_Matching(curr_rel);
		}
	}
	
	public static void FillDependentRulesData_TrainingData()
	{
		for(String rel_id: Data.map_TrainingRelations.keySet())
		{
			Relation curr_rel = Data.map_TrainingRelations.get(rel_id);
			DedependentRule_Relevant_Matching(curr_rel);
		}
	}
	
	
	private static void DedependentRule_Relevant_Matching(Relation curr_rel)
	{
		
		String Rel_id = curr_rel.getRelation_Id();
		String curr_sourceConcept = curr_rel.getSource_Concept();
		String curr_targetConcept = curr_rel.getTarget_Concept();
		List<String> curr_verbs = (Data.map_relationIDs_VBs.get(Rel_id) == null)?new ArrayList<String>():Data.map_relationIDs_VBs.get(Rel_id);
		List<String> curr_labelNPs = (Data.map_relationIDs_NPs.get(Rel_id) == null)?new ArrayList<String>():Data.map_relationIDs_NPs.get(Rel_id);
		boolean isCurrRelationAssociation = (curr_rel.getType().equals(RelationType.ASSOCIATION));
		
		for(String prev_rel_id: Data.map_TrainingRelations.keySet())
		{
			//Match only upto the currrent relation (Since the maps is a linked map, then the order is maintained)
			if(prev_rel_id.equals(Rel_id))
			{
				continue;
			}
			
			//Data for a previously seen relation
			Relation prev_rel = Data.map_TrainingRelations.get(prev_rel_id);
			int prev_rel_Relevant = (prev_rel.getIsRelevant().equals(RelevanceType.YES)?1:0);
			int prev_rel_IrRelevant = (prev_rel.getIsRelevant().equals(RelevanceType.NO)?1:0);
			
			boolean isPrevRelationAssociation = (prev_rel.getType().equals(RelationType.ASSOCIATION));			
			String prev_sourceConcept = prev_rel.getSource_Concept();
			String prev_targetConcept = prev_rel.getTarget_Concept();
			List<String> list_prevVerbs = (Data.map_relationIDs_VBs.get(prev_rel_id) == null)?new ArrayList<String>():Data.map_relationIDs_VBs.get(prev_rel_id);
			List<String> list_prevlabelNPs = (Data.map_relationIDs_NPs.get(prev_rel_id) == null)?new ArrayList<String>():Data.map_relationIDs_NPs.get(prev_rel_id);
			
			//Match source and target of current relation (R) to source and target of a previously seen relation (R')
			String src_src_Match = Similarity_Utils.doStringsfuzzyMatch_MorePrecise(curr_sourceConcept, prev_sourceConcept);
			String src_target_Match = Similarity_Utils.doStringsfuzzyMatch_MorePrecise(curr_sourceConcept, prev_targetConcept);
			String target_src_Match = Similarity_Utils.doStringsfuzzyMatch_MorePrecise(curr_targetConcept, prev_sourceConcept);
			String target_target_Match = Similarity_Utils.doStringsfuzzyMatch_MorePrecise(curr_targetConcept, prev_targetConcept);
			
			boolean srcMatched = false;
			boolean targetMatched = false;
			
			//Source Matched
			if(!src_src_Match.isEmpty() || !src_target_Match.isEmpty())
			{
				srcMatched = true;
				
				if(Data.map_relationIDs_matchingSourcetoAny.containsKey(Rel_id))
		    	{
					HashSet<String> list_Relation = Data.map_relationIDs_matchingSourcetoAny.get(Rel_id);
					list_Relation.add(prev_rel_id);
					Data.map_relationIDs_matchingSourcetoAny.put(Rel_id,list_Relation);
					//UpdateRelevanceCounts
					curr_rel.setnum_RelevantSharedSource(curr_rel.getnum_RelevantSharedSource() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSharedSource(curr_rel.getnum_IrRelevantSharedSource() + prev_rel_IrRelevant);
		    	}
		    	else
		    	{
		    		HashSet<String> list_Relation = new HashSet<String>();
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_matchingSourcetoAny.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSharedSource(curr_rel.getnum_RelevantSharedSource() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSharedSource(curr_rel.getnum_IrRelevantSharedSource() + prev_rel_IrRelevant);
		    	}							
			}
			
			//Target Matched
			if(!target_src_Match.isEmpty() || !target_target_Match.isEmpty())
			{
				targetMatched = true;
				if(Data.map_relationIDs_matchingTargettoAny.containsKey(Rel_id))
		    	{
					HashSet<String> list_Relation = Data.map_relationIDs_matchingTargettoAny.get(Rel_id);
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_matchingTargettoAny.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSharedTarget(curr_rel.getnum_RelevantSharedTarget() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSharedTarget(curr_rel.getnum_IrRelevantSharedTarget() + prev_rel_IrRelevant);
		    	}
		    	else
		    	{
		    		HashSet<String> list_Relation = new HashSet<String>();
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_matchingTargettoAny.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSharedTarget(curr_rel.getnum_RelevantSharedTarget() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSharedTarget(curr_rel.getnum_IrRelevantSharedTarget() + prev_rel_IrRelevant);
		    	}			
			}
			
			//Both Source and Target Matched
			if(srcMatched && targetMatched)
			{
				if(Data.map_relationIDs_matchingEndConcepts.containsKey(Rel_id))
		    	{
					HashSet<String> list_Relation = Data.map_relationIDs_matchingEndConcepts.get(Rel_id);
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_matchingEndConcepts.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSameEndConcepts(curr_rel.getnum_RelevantSameEndConcepts() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSameEndConcepts(curr_rel.getnum_IrRelevantSameEndConcepts() + prev_rel_IrRelevant);
		    	}
		    	else
		    	{
		    		HashSet<String> list_Relation = new HashSet<String>();
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_matchingEndConcepts.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSameEndConcepts(curr_rel.getnum_RelevantSameEndConcepts() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSameEndConcepts(curr_rel.getnum_IrRelevantSameEndConcepts() + prev_rel_IrRelevant);
		    	}			
			}
			
			//Match Verbs			
			boolean verbsMatched = Similarity_Utils.doListofStringsfuzzyMatch_MorePrecise(curr_verbs, list_prevVerbs);
			if(verbsMatched && (!curr_rel.getReq_Id().equals(prev_rel.getReq_Id())) && isCurrRelationAssociation && isPrevRelationAssociation)
			{
				if(Data.map_relationIDs_matchingVerbs.containsKey(Rel_id))
		    	{
					HashSet<String> list_Relation = Data.map_relationIDs_matchingVerbs.get(Rel_id);
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_matchingVerbs.put(Rel_id,list_Relation);	
		    		curr_rel.setnum_RelevantSameVerbs(curr_rel.getnum_RelevantSameVerbs() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSameVerbs(curr_rel.getnum_IrRelevantSameVerbs() + prev_rel_IrRelevant);
		    	}
		    	else
		    	{
		    		HashSet<String> list_Relation = new HashSet<String>();
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_matchingVerbs.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSameVerbs(curr_rel.getnum_RelevantSameVerbs() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSameVerbs(curr_rel.getnum_IrRelevantSameVerbs() + prev_rel_IrRelevant);
		    	}			
			}
			
			//Match the NPs in relation Labels
			boolean relationNPsmatched = Similarity_Utils.doListofStringsfuzzyMatch_MorePrecise(curr_labelNPs, list_prevlabelNPs);
			if(relationNPsmatched && isCurrRelationAssociation && isPrevRelationAssociation)
			{
				if(Data.map_relationIDs_matchingRelationNPs.containsKey(Rel_id))
		    	{
					HashSet<String> list_Relation = Data.map_relationIDs_matchingRelationNPs.get(Rel_id);
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_matchingRelationNPs.put(Rel_id,list_Relation);
		    	}
		    	else
		    	{
		    		HashSet<String> list_Relation = new HashSet<String>();
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_matchingRelationNPs.put(Rel_id,list_Relation);		    		
		    	}			
			}
			
			/*
			 * MATCH if a previous relation is a subset/superset of the current relation in terms of phrases (Source, target, VBs and NPs in Label)
			 */
			List<String> list_prev = new ArrayList<>();
			list_prev.add(prev_sourceConcept);
			list_prev.add(prev_targetConcept);
			list_prev.addAll(list_prevVerbs);
			list_prev.addAll(list_prevlabelNPs);
			
			List<String> list_curr = new ArrayList<>();
			list_curr.add(curr_sourceConcept);
			list_curr.add(curr_targetConcept);
			list_curr.addAll(curr_verbs);
			list_curr.addAll(curr_labelNPs);
			//SUBSET
			boolean isPrevRelationSUBSETofCurrRelation = Similarity_Utils.is_lst1_a_Subsetof_lst2(list_prev, list_curr);
			if(isPrevRelationSUBSETofCurrRelation)
			{
				if(Data.map_relationIDs_subsetRelations.containsKey(Rel_id))
		    	{
					HashSet<String> list_Relation = Data.map_relationIDs_subsetRelations.get(Rel_id);
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_subsetRelations.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSubset(curr_rel.getnum_RelevantSubset() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSubset(curr_rel.getnum_IrRelevantSubset() + prev_rel_IrRelevant);
		    	}
		    	else
		    	{
		    		HashSet<String> list_Relation = new HashSet<String>();
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_subsetRelations.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSubset(curr_rel.getnum_RelevantSubset() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSubset(curr_rel.getnum_IrRelevantSubset() + prev_rel_IrRelevant);
		    	}			
			}
			//SUPERSET
			boolean isPrevRelationSUPERSETofCurrRelation = Similarity_Utils.is_lst1_a_Subsetof_lst2(list_curr, list_prev);
			if(isPrevRelationSUPERSETofCurrRelation)
			{
				if(Data.map_relationIDs_supersetRelations.containsKey(Rel_id))
		    	{
					HashSet<String> list_Relation = Data.map_relationIDs_supersetRelations.get(Rel_id);
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_supersetRelations.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSuperset(curr_rel.getnum_RelevantSuperset() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSuperset(curr_rel.getnum_IrRelevantSuperset() + prev_rel_IrRelevant);
		    	}
		    	else
		    	{
		    		HashSet<String> list_Relation = new HashSet<String>();
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_supersetRelations.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantSuperset(curr_rel.getnum_RelevantSuperset() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantSuperset(curr_rel.getnum_IrRelevantSuperset() + prev_rel_IrRelevant);
		    	}			
			}
			

			/*
			 * Same Requirement Relations
			 */
			boolean isReqSame = (curr_rel.getReq_Id().equals(prev_rel.getReq_Id()));
			boolean isRuleSame = (curr_rel.getRule_Id().equals(prev_rel.getRule_Id()));
			
			if(isReqSame)
			{
				curr_rel.setnum_RelationsinSameReq(curr_rel.getnum_RelationsinSameReq() + 1);				
				if(Data.map_relationIDs_sameReqRelations.containsKey(Rel_id))
		    	{
					HashSet<String> list_Relation = Data.map_relationIDs_sameReqRelations.get(Rel_id);
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_sameReqRelations.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantinSameReq(curr_rel.getnum_RelevantinSameReq() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantinSameReq(curr_rel.getnum_IrRelevantinSameReq() + prev_rel_IrRelevant);
		    	}
		    	else
		    	{
		    		HashSet<String> list_Relation = new HashSet<String>();
		    		list_Relation.add(prev_rel_id);
		    		Data.map_relationIDs_sameReqRelations.put(Rel_id,list_Relation);
		    		curr_rel.setnum_RelevantinSameReq(curr_rel.getnum_RelevantinSameReq() + prev_rel_Relevant);
					curr_rel.setnum_IrRelevantinSameReq(curr_rel.getnum_IrRelevantinSameReq() + prev_rel_IrRelevant);
		    	}				
			}
			

			
			if(isReqSame && isRuleSame)
			{
					if(Data.map_relationIDs_sameReqsameRuleRelations.containsKey(Rel_id))
					{
						HashSet<String> list_Relation = Data.map_relationIDs_sameReqsameRuleRelations.get(Rel_id);
						list_Relation.add(prev_rel_id);
						Data.map_relationIDs_sameReqsameRuleRelations.put(Rel_id,list_Relation);
						curr_rel.setnum_RelevantinSameReq_sameRule(curr_rel.getnum_RelevantinSameReq_sameRule() + prev_rel_Relevant);
						curr_rel.setnum_IrRelevantinSameReq_sameRule(curr_rel.getnum_IrRelevantinSameReq_sameRule() + prev_rel_IrRelevant);
					}
					else
					{
						HashSet<String> list_Relation = new HashSet<String>();
						list_Relation.add(prev_rel_id);
						Data.map_relationIDs_sameReqsameRuleRelations.put(Rel_id,list_Relation);
						curr_rel.setnum_RelevantinSameReq_sameRule(curr_rel.getnum_RelevantinSameReq_sameRule() + prev_rel_Relevant);
						curr_rel.setnum_IrRelevantinSameReq_sameRule(curr_rel.getnum_IrRelevantinSameReq_sameRule() + prev_rel_IrRelevant);
					}			
				}				
		}	
	}	
//	private static boolean isSameLinkPathGroup(String curr_rel_Id, String prev_rel_Id)
//	{
//		Relation current_rel = Data.map_AllRelations.get(curr_rel_Id);
//
//			if(Data.map_LPgroups.containsKey(current_rel.getReq_Id()))		
//			{
//				List<List<String>> list_LPgroups = Data.map_LPgroups.get(current_rel.getReq_Id());
//				for(List<String> LPgroup: list_LPgroups)
//				{
//					if(LPgroup.contains(curr_rel_Id) && LPgroup.contains(prev_rel_Id))
//					{
//						return true;
//					}
//				}
//			}
//			return false;
//	}
}
