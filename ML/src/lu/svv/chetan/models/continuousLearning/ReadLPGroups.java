package lu.svv.chetan.models.continuousLearning;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/*
 * This class has been created to read the LinkPath groups (e.g., R2 --> R2.1, R2.2) 
 * The idea is  to see -- usually only one relation from a LP group is relevant and thus we can focus on Linkpath groups as a learning feature
 */
public class ReadLPGroups {
	
	/*
	 * This function is there just to test the functioning of the  other public functions in this class.
	 */
	public static void main(String[] args)
	{
		readRelationsFile("/Users/chetan.arora/Documents/workspace_practice/RelevLearning_Models/res/relation_docs/SES_LPGroups.csv");
	}
	
	/*
	 * Read the file with Requirement IDs and corresponding Relations
	 * 
	 * ***Public variables updated**
	 * 		
	 */
	public static void readRelationsFile(String fileName) {		
		  FileReader file = null;
		  BufferedReader reader = null;		  

		  try {
		    file = new FileReader(fileName);
			reader = new BufferedReader(file);
		    String line = "";
		    int header = 0;
		    while ((line = reader.readLine()) != null) {
		    	if(header == 0)
		    	{
		    		header++;
		    		continue;
		    	}
		    	String [] relationsLine = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
		    	String req_Id = relationsLine[0].trim();
		    	String [] grouped_Relations = relationsLine[1].trim().split(";"); //
		    	if(Data.map_LPgroups.containsKey(req_Id))
		    	{
		    		Data.map_LPgroups.get(req_Id).add(Arrays.asList(grouped_Relations));
		    	}
		    	else
		    	{
		    		List<List<String>> list_LPgroups = new ArrayList<>();
		    		list_LPgroups.add(Arrays.asList(grouped_Relations));
		    		Data.map_LPgroups.put(req_Id, list_LPgroups);
		    	}
		    }
		  }catch (Exception e) {
			  System.out.println("File(s) not Found" + System.lineSeparator());
			  
		      throw new RuntimeException(e);
		  } finally {
		    if (file != null) {
		      try {
		    	  file.close();
		      } catch (IOException e) {
		        // Ignore issues during closing 
		      }
		    }
		  }
	}

}
