package lu.svv.chetan.models.continuousLearning;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RserveException;

import gate.Corpus;
/*
 * This class is like a recommendation system, you don't filter out anything.
 * But recommend if something is NO,
 * After rejection in ESEM -- we decided to try new algorithm where you don't watch something so many times, instead wait until the middle uncertain area is clear 
 */
public class MainClass_2Learners_Recommendation_BoundaryValueAnalysis {
	
	//Fixed variables
	static String wd = "/Users/chetan.arora/Dropbox/PhD Folder/work/Experimentation/Model_Extraction_ML/";
	static String Relations_FileName = "OpenCoss_Interview_Data_learning";
	static String directory = new String();
	static Logger logger = Logger.getLogger(MainClass_2Learners_Recommendation_BoundaryValueAnalysis.class.getName());
	
	//Algorithm specific variables
	static int number_RelationsperRound = 1; //Number of relations to be added to the training set (show to the user) per round, by default
	static int num_minRoundsBeforeDecision = 0; //Num of min rounds before adding NO relations directly to the training set, without confirming		
	static int minScoreforDecision = 5; //Min score for making a NO recommendation
	static StringBuilder sb = new StringBuilder();
	static StringBuilder log_sb = new StringBuilder(); 
	static double YES_Threshold = 0.65;
	static double NO_Threshold = 0.0;	
	static double K_Value = 0.15; //Value for the upper limit of min score for decision
	
	//Per Algorithm Run variables
	static LinkedHashMap<String, Integer> watchList_NO = new LinkedHashMap<>();
	static LinkedHashMap<String, Integer> watchList_YES = new LinkedHashMap<>();
	static LinkedHashMap<String, Integer> watchList_DIFF = new LinkedHashMap<>();
	public static int curr_Iteration = 0; //The number of iterations of the whole algo
	static List<String> finalPredictedRelations = new ArrayList<>();
	static List<Relation> lst_wronglyClassifiedRelations = new ArrayList<>();
	
	static int correctSoFar_NO = 0;
	static int num_RelevantinInitialTraining = 0;
	static int num_IrRelevantinInitialTraining = 0;
	
	static int num_R_Associations = 0;
	static int num_IR_Associations = 0;
	static int num_R_Aggregations = 0;
	static int num_IR_Aggregations = 0;
	static int num_R_Generalizations = 0;
	static int num_IR_Generalizations = 0;
	
	//Per iteration variables 
	static int roundNumber = 0; //Current round number
	static int maxPossibleRounds = 0;
	
	static double initialSet40 = 0.4;
	static long startTime = System.currentTimeMillis();
	
	//COunt the number of relations correctly predicted by LP Groups
	static int LPGroups_Correct = 0;
	static int LPGroups_InCorrect = 0;
	
	public static void main(String [] args) throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		/*
		 * Initialize the GATE framework and prepare the data maps for requirements and relations
		 */
		init();

		double [] NO_Thresholds = {0.05, 0.1};
		double [] YES_Thresholds = {0.05, 0.1};
		//double [] K_Values = {0.1, 0.15, 0.2};
		double [] K_Values = {0.05, 0, 5};
		for(double K: K_Values)
		{		
			for(double NO: NO_Thresholds)
			{
				for(double YES: YES_Thresholds)
				{
					NO_Threshold = NO;
					YES_Threshold = YES;
					K_Value = K;
					
					/*
					 * Write thresholds data to a file, so that R can read it
					 */				
					WriteData.writeSimpleData(YES_Threshold + System.lineSeparator() + NO_Threshold, wd + "Thresholds.csv", false);
					
					executeExperimentSet();
				}
			}
		}
		System.out.println((System.currentTimeMillis() - startTime)/1000 + " seconds");
	}
	

	/*
	 * Initialize the directory to write all data
	 * Start GATE
	 * READ Requirements
	 * Prepare hashmaps for Relations and corresponding phrases
	 */
	private static void init()  throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		//Create a directory to store all the files for this run		
		File f = new File(wd + "Executions_July2017_AfterRejection/" + "Experiment_Recommendation_newK_" + "OpenCoss_Single_Reduced_BVA_" + initialSet40);
		f.mkdir();
		directory = f.getAbsolutePath() + "/";
		System.out.println(directory);
		
		/*
		 * Read Requirements Text
		 */
			String Requirements_FileName = "OpenCoss_Data_Requirements";
			ReadCSV.readReqsTextFile("./res/req_docs/" + Requirements_FileName + ".csv");
			
			/*
			 * Process Requirements Text and find the NPs and VBs in each requirement
			 */
				try {
					Corpus corpus = GATE_NLP.init();
					System.out.println(corpus.getDocumentName(0));
					GATE_NLP.extractData(corpus);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				/*
				 * Read Relations Text
				 */								
					ReadCSV.readRelationsFile("./res/relation_docs/" + Relations_FileName + ".csv");
					
					ReadLPGroups.readRelationsFile("./res/relation_docs/OpenCoss_LPGroups.csv");
							
					/*
					 * Get Phrases for the association relations labels
					 * 
					 * ***Public variables updated**
					 * 		Data.map_relationIDs_VBs
					 * 		Data.map_relationIDs_NPs
					 */
					for(String reqID: Data.map_reqIDs_Relations.keySet())
					{
						List<Relation> list_Relations = Data.map_reqIDs_Relations.get(reqID);
						List<String> list_VBs = Data.map_reqIDs_VBs.get(reqID);
						List<String> list_NPs = Data.map_reqIDs_NPs.get(reqID);
						for(Relation rel: list_Relations)
						{
							if(!rel.getRelation_Text().trim().equals(""))
							{
								List<String> lst_VBs = DependentRules_Utils.getLabelPhrases(rel.getRelation_Text().trim(), list_VBs);
								List<String> lst_NPs = DependentRules_Utils.getLabelPhrases(rel.getRelation_Text().trim(), list_NPs);
								if(lst_VBs.size() > 0)
									Data.map_relationIDs_VBs.put(rel.getRelation_Id(), lst_VBs);
								if(lst_NPs.size() > 0)
								Data.map_relationIDs_NPs.put(rel.getRelation_Id(), lst_NPs);
							}
						}				 
					}					
	}
	
	private static void executeExperimentSet() throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		/*
		 * The iterations for the number of time each setting needs to be run
		 */
		for(int iteration = 21; iteration<=25; iteration++)
		{
			curr_Iteration = iteration;

		//Step 1 - Initialize the first XX% data, by picking a relation per requirement		
		log_sb.append("*** Staring Round OpenCoss" + NO_Threshold + " - initial = " + initialSet40 + " - NO = " + iteration + " ****" + System.lineSeparator());
		
		Pick_InitialTrainingSet.diversifiedOrder_byDistributionofRelationsTypes(curr_Iteration, InitalSet_Numbers.num_Aggregation_OpenCoss_initialSet40, 
																										 InitalSet_Numbers.num_Generalization_OpenCoss_initialSet40,
																										 InitalSet_Numbers.num_Attributes_OpenCoss,
																										 initialSet40);			
		countNumRelevantRelationsinInitialSet();
		maxPossibleRounds = Data.map_TestRelations.size();
		
		log_sb.append(Utilities.listToString((Collection<String>)Data.map_TrainingRelations.keySet(), " - "));
		log_sb.append(System.lineSeparator());
		log_sb.append("*** Round " + curr_Iteration + "_" + roundNumber + " ***" + System.lineSeparator());
		
		//Step 2 - Initialize the watchlists
		for(String key: Data.map_TestRelations.keySet())
		{
			watchList_NO.put(key, 0);
			watchList_YES.put(key, 0);
			watchList_DIFF.put(key, 0);
		}				
		 // Step 2.1 -- Update the LP Group markings for the relations chosen in the initial training set -- It updates their watch lists
		for(String rel_Id: Data.map_TrainingRelations.keySet())
		{
			updateLinkPathGroups(rel_Id);
		}
		
		//Step 3 - First Iteration
		singleIteration(); 

		//Step 4 -- Reduced Subsequent Iterations
		while((watchList_DIFF.size() > (0.01 * Data.map_AllRelations.size())) && (Data.map_TrainingRelations.size() < Data.map_AllRelations.size()))
		{	
			WriteData.writeSimpleData(curr_Iteration + System.lineSeparator() + roundNumber, wd + "iteration.csv", false);	
			log_sb.append("*** Round " + curr_Iteration + "_" + roundNumber + " ***" + System.lineSeparator());
			
			Data.reset();						
			pickNextCandidatesforTraining();
			
			singleIteration();						
			logger.log(Level.INFO, "***Round " + roundNumber + " completed with " + watchList_DIFF.size() + " in the watchlist and number of elements in automatic NO recommendation =" + finalPredictedRelations.size() + "and correct = " + correctSoFar_NO);	
			
			roundNumber++;			
			WriteData.writeSimpleData(log_sb.toString(), directory + "AA_LOG.csv", true);			
			log_sb = new StringBuilder();
			
			System.out.println("ROUND " + iteration + " - " + roundNumber + " finished ** and took " + (System.currentTimeMillis() - startTime)/1000 + " seconds");
		}
		
		//Step 5 - Post-Mortem (1 Algorithm Run Finished)
			computeAccuracy();
			WriteData.writeSimpleData(sb.toString(), directory + "AAccuracy_continuousLearning.csv", true);
		
			//Step 6 - RESET Variables
			resetAlgorithmRunVariables();
		}
		WriteData.writeSimpleData(log_sb.toString(), directory + "AA_LOG.csv", false);
	}
	
	private static void countNumRelevantRelationsinInitialSet()
	{
		int num_IrRelevant = 0;
		for(String key: Data.map_TrainingRelations.keySet())
		{
			Relation rel = Data.map_TrainingRelations.get(key);
			if(rel.getIsRelevant().equals(RelevanceType.NO))
			{
				num_IrRelevant++;
			}
			
			if(rel.getType().equals(RelationType.ASSOCIATION))
			{
				if(rel.getIsRelevant().equals(RelevanceType.NO))
				{
					num_IR_Associations++;
				}
				else
				{
					num_R_Associations++;
				}
				
			}
			else if(rel.getType().equals(RelationType.AGGREGATION))
			{
				if(rel.getIsRelevant().equals(RelevanceType.NO))
				{
					num_IR_Aggregations++;
				}
				else
				{
					num_R_Aggregations++;
				}				
			}
			else if(rel.getType().equals(RelationType.GENERALIZATION))
			{
				if(rel.getIsRelevant().equals(RelevanceType.NO))
				{
					num_IR_Generalizations++;
				}
				else
				{
					num_R_Generalizations++;
				}				
			}
		}
		num_IrRelevantinInitialTraining = num_IrRelevant;
		num_RelevantinInitialTraining = Data.map_TrainingRelations.size() - num_IrRelevant;
		log_sb.append("Total Relations selected in initial Training Set = " + Data.map_TrainingRelations.size() + System.lineSeparator());
	
		log_sb.append("Total Aggregations selected in initial Training Set = " + num_R_Aggregations + num_IR_Aggregations  + System.lineSeparator());
		log_sb.append("Aggregations Split = " + num_R_Aggregations + " R / IR " + num_IR_Aggregations + System.lineSeparator());
		
		log_sb.append("Total Generalizations selected in initial Training Set = " + num_R_Generalizations + num_IR_Generalizations + System.lineSeparator());
		log_sb.append("Generalizations Split = " + num_R_Generalizations + " R / IR " + num_IR_Generalizations + System.lineSeparator());
		
		log_sb.append("Total Associations selected in initial Training Set = " + num_R_Associations + num_IR_Associations + System.lineSeparator());
		log_sb.append("Associations Split = " + num_R_Associations + " R / IR " + num_IR_Associations + System.lineSeparator());
	}
	
	private static void singleIteration() throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		FillDependentRulesData.FillDependentRulesData_TrainingData();
		FillDependentRulesData.FillDependentRulesData_TestData();	
		
		String str_trainingFile = directory + Relations_FileName + "_TrainingSet.csv";
		String str_testFile = directory + Relations_FileName + "_TestSet.csv";
		
		WriteData.writeCSV(Data.map_TrainingRelations, wd + Relations_FileName + "_TrainingSet_0.csv", str_trainingFile);
		WriteData.writeCSV(Data.map_TestRelations, wd + Relations_FileName + "_TestSet_0.csv", str_testFile);
		
		WriteData.writeSimpleData(str_trainingFile + System.lineSeparator() + str_testFile, wd + "filename.csv", false);
		System.out.println((System.currentTimeMillis() - startTime)/1000 + " seconds");
		if(Data.map_TestRelations.size() > 0)
		{
			RList rl = R_Java_Interface.callRInterface();
			//Call the R Code
			String [] vector_MatchingNO = getMatchingRelationsFromRList(rl, 1, 4, "NO");
			String [] vector_MatchingYES = getMatchingRelationsFromRList(rl, 1, 4, "YES");
			updateWatchlist(vector_MatchingNO, vector_MatchingYES);
		}
		System.out.println((System.currentTimeMillis() - startTime)/1000 + " seconds");
	}
	
	private static void resetAlgorithmRunVariables()
	{
		watchList_NO = new LinkedHashMap<>();
		watchList_YES = new LinkedHashMap<>();
		watchList_DIFF = new LinkedHashMap<>();		
		roundNumber = 0; //Current round number
		
		finalPredictedRelations = new ArrayList<>();
		lst_wronglyClassifiedRelations = new ArrayList<>();
		Data.reset();
		Data.map_TrainingRelations = new LinkedHashMap<>();
		Data.map_TestRelations = new LinkedHashMap<>();
		for(String rel_Id: Data.map_AllRelations.keySet())
		{
			Relation rel = Data.map_AllRelations.get(rel_Id);
			rel.setIsRelevant(Data.map_relationIDs_Relevance.get(rel_Id));
		}
		sb = new StringBuilder();
		log_sb = new StringBuilder();
		
		correctSoFar_NO = 0;
		num_IrRelevantinInitialTraining = 0;
		num_RelevantinInitialTraining = 0;
		num_R_Associations = 0;
		num_IR_Associations = 0;
		num_R_Aggregations = 0;
		num_IR_Aggregations = 0;
		num_R_Generalizations = 0;
		num_IR_Generalizations = 0;
		LPGroups_Correct = 0;
		LPGroups_InCorrect = 0;
	}
		
	private static void pickNextCandidatesforTraining()
	{
		int i = 0;
		boolean didWeAddAnythingAutomatically = false;
		Iterator<String> it_keys = watchList_DIFF.keySet().iterator();
		
		/*
		 * Strength of Evidence -- Dynamic
		 */		
		double x = (maxPossibleRounds - watchList_DIFF.size()) * (100.0/maxPossibleRounds);	
		double upper_Limit = maxPossibleRounds * K_Value;
		double lower_Limit = 5;
		
		//Formular for Slope of a line
		minScoreforDecision = (int) Math.round(upper_Limit - ((x/95) * (upper_Limit - lower_Limit)));  
		
		if(K_Value == 0)
		{
			minScoreforDecision = 1;
		}
		else if(K_Value == 5)
		{
			minScoreforDecision = 5;
		}
			
		/*
		 * Steps for the min diff score required before recommending a relation
		 */
//		if(x>95)		{
//			minScoreforDecision = 5;
//		}
//		else if(x>80 && x<95) {
//			minScoreforDecision = 7;
//		}
//		else if(x > 50 && x<80) {
//			minScoreforDecision = 10;
//		}
//		else if(x > 30 && x<50) {
//			minScoreforDecision = 14;
//		}
//		else {
//			minScoreforDecision = 20;
//		}		
		
////		double a = (maxPossibleRounds - (roundNumber * number_RelationsperRound))/5;
////		minScoreforDecision = (int) Math.max(a, 5);
			System.out.println("Strength of Evidence = " + minScoreforDecision);
		
		/*
		 * The logic for adding relations directly to the training set (NOT shown to the user)
		 */
		if(roundNumber >= num_minRoundsBeforeDecision)
		{
			while(it_keys.hasNext())
			{
				String rel_Id = it_keys.next();
				int curr_diff_score = watchList_DIFF.get(rel_Id);
				int curr_NO_score = watchList_NO.get(rel_Id);
				int curr_YES_score = watchList_YES.get(rel_Id);
				
				
				if((curr_diff_score >= minScoreforDecision) && (curr_NO_score > curr_YES_score) && (curr_YES_score <=3))
				{					
					moveRelationfromTestToTraining(rel_Id);					
					didWeAddAnythingAutomatically = true;
						
					updatePredictionsSoFar(rel_Id);
					it_keys.remove();

					logger.log(Level.INFO, "Automatically Added:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(rel_Id));
					log_sb.append("Automatically Added:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(rel_Id) + System.lineSeparator());
					if(Data.map_relationIDs_Relevance.get(rel_Id).equals(RelevanceType.YES))
					{
						lst_wronglyClassifiedRelations.add(Data.map_AllRelations.get(rel_Id));
					}
				}
				else if(curr_diff_score > 1 && curr_YES_score > 3)
				{
					didWeAddAnythingAutomatically = true;
					logger.log(Level.INFO, "Default Addition continued:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score);
					log_sb.append("Default Addition continued:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + System.lineSeparator());
					
					moveRelationfromTestToTraining(rel_Id);
					it_keys.remove();								
				}
			}
		}
		
		//We prefer picking relations that are either indecisive or tilted towards YES
		boolean requiredDefaultRelationsFound = false;
		
		/*
		 * Logic for showing relations to the user by default 
		 */
		if(didWeAddAnythingAutomatically == false)
		{
			it_keys = watchList_DIFF.keySet().iterator();
			while(it_keys.hasNext())
			{			
				String rel_Id = it_keys.next();
				int curr_NO_score = watchList_NO.get(rel_Id);
				int curr_YES_score = watchList_YES.get(rel_Id);
				
				if(curr_YES_score >= curr_NO_score) // Tilted towards YES
				{					
					moveRelationfromTestToTraining(rel_Id);
					it_keys.remove();
					logger.log(Level.INFO, "Default Added:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(rel_Id));					
					log_sb.append("Default Added:: " + rel_Id + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(rel_Id) + System.lineSeparator());
					i++;
				}
				
				if(i == number_RelationsperRound)
				{
					requiredDefaultRelationsFound = true;
					break;
				}
			}
			it_keys = watchList_DIFF.keySet().iterator();
			if(requiredDefaultRelationsFound == false) //Indecisive
			{
				while(it_keys.hasNext())
				{			
					String rel_Id = it_keys.next();
					int curr_NO_score = watchList_NO.get(rel_Id);
					int curr_YES_score = watchList_YES.get(rel_Id);
			
						moveRelationfromTestToTraining(rel_Id);
						it_keys.remove();
						logger.log(Level.INFO, "Default Added:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(rel_Id));
						log_sb.append("Default Added:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(rel_Id) + System.lineSeparator());
						i++;
					
					if(i == number_RelationsperRound)
					{
						break;
					}
				}
			}
		}
	}
	
	private static void moveRelationfromTestToTraining(String rel_Id)
	{
		Relation current_rel = Data.map_AllRelations.get(rel_Id);
				
			Data.map_TrainingRelations.put(rel_Id, current_rel);
			Data.map_TestRelations.remove(rel_Id);
			//Update the LinkPath Group 
			updateLinkPathGroups(rel_Id);
	}
	
	private static void updatePredictionsSoFar(String rel_Id)
	{
		finalPredictedRelations.add(rel_Id);
		if(Data.map_relationIDs_Relevance.get(rel_Id).equals(RelevanceType.NO))
		{
				correctSoFar_NO++;
		}	
	}
	
	/*
	 * There are multiple ways of updating this, but the easiest and the most non intrusive way is to update the watchlist for all the other relations in the LINKPATH Group 
	 */
	private static void updateLinkPathGroups(String rel_Id)
	{
		Relation current_rel = Data.map_AllRelations.get(rel_Id);
		//Check LP Groups Rule -- LP Groups rule is that if one relation from a LP group has been marked relevant put others from the group as irrelevant		
		if(current_rel.getIsRelevant().equals(RelevanceType.YES))
		{
			if(Data.map_LPgroups.containsKey(current_rel.getReq_Id()))		
			{
				List<List<String>> list_LPgroups = Data.map_LPgroups.get(current_rel.getReq_Id());
				for(List<String> LPgroup: list_LPgroups)
				{
					if(LPgroup.contains(rel_Id))
					{
						//Recommend all the other elements remaining in the test set to the training set as irrelevant
						for(String LP_Rel: LPgroup)
						{
							if(Data.map_TestRelations.containsKey(LP_Rel) && !LP_Rel.equals(rel_Id))
							{
								watchList_DIFF.put(LP_Rel, 100);
								watchList_YES.put(LP_Rel, 1);
								watchList_NO.put(LP_Rel, 101);
								Relation r = Data.map_TestRelations.get(LP_Rel);
								if(r.getIsRelevant().equals(RelevanceType.YES))
								{
									LPGroups_InCorrect++;
								}
								else
								{
									LPGroups_Correct++;
								}
							}
						}
					}
				}
			}
		}
	}
	
	/*
	 * This is the function to sort a hashmap
	 */
	public static <K, V extends Comparable<? super V>> LinkedHashMap<K, V> sortByValue(LinkedHashMap<K, V> map) {
	    return map.entrySet()
	              .stream()
	              .sorted(HashMap.Entry.comparingByValue(/*Collections.reverseOrder()*/))
	              .collect(Collectors.toMap(
	            		  HashMap.Entry::getKey, 
	            		  HashMap.Entry::getValue, 
	                (e1, e2) -> e1, 
	                LinkedHashMap::new
	              ));
	}
	
		
	/*
	 * Update the YES and NO watchlists
	 */
	private static void updateWatchlist(String [] vector_MatchingNO, String [] vector_MatchingYES)
	{
		for(String str: vector_MatchingNO)
		{
			int prev_Score = watchList_NO.get(str);
			watchList_NO.put(str, prev_Score + 1);
			watchList_DIFF.put(str, Math.abs(watchList_NO.get(str) - watchList_YES.get(str)));
			//watchList_DIFF.put(str, watchList_NO.get(str) - watchList_YES.get(str));
		}
		
		for(String str: vector_MatchingYES)
		{
			int prev_Score = watchList_YES.get(str);
			watchList_YES.put(str, prev_Score + 1);
			watchList_DIFF.put(str, Math.abs(watchList_NO.get(str) - watchList_YES.get(str)));
			//watchList_DIFF.put(str, (watchList_NO.get(str) - watchList_YES.get(str)));
		}
		//Sort the difference watchlist in an ascending order
		watchList_DIFF = sortByValue(watchList_DIFF);
	}

	/*
	 * This fuctions returns the array of relations that match YES/NO probabilities predicted greater than a threshold
	 */
	private static String [] getMatchingRelationsFromRList(RList rlist, int index_relations, int index_prob, String result) throws REXPMismatchException
	{
		String[] vector_results = rlist.at(index_prob).asStrings(); // 4 0r 5		
		String[] vector_RelIDs = rlist.at(index_relations).asStrings(); //1
		
		int[] prob_indices = IntStream.range(0, vector_results.length)
                .filter(i -> vector_results[i].equals(result))
                .toArray();
		
		String [] filtered_Relations_prob = new String[prob_indices.length];
		
		int j = 0;
		for (int i : prob_indices) {
			filtered_Relations_prob[j] = vector_RelIDs[i];
			j++;
		}		
		return filtered_Relations_prob;
	}

	private static void computeAccuracy()
	{
		int noCount = 0;
		for(String relId: finalPredictedRelations)
		{
			if(Data.map_relationIDs_Relevance.get(relId).equals(RelevanceType.NO))
			{
				noCount++;
			}
		}
		logger.log(Level.INFO, "**** We got " + noCount + " NO relations right out of " + finalPredictedRelations.size() + " ****");
		
		writeResults(noCount);
		//sb.append(curr_Iteration + "," + num_RelevantinInitialTraining + "," + num_IrRelevantinInitialTraining + ",NO," + noCount + "," + finalPredictedRelations.size() + System.lineSeparator());
		log_sb.append("**** We got " + noCount + " NO relations right out of " + finalPredictedRelations.size() + " ****");
		
		for(Relation rel: lst_wronglyClassifiedRelations)
		{
			String relId = rel.getRelation_Id();
			logger.log(Level.INFO, "Wrongly Classified Relation " + relId + " -- Relation Type:: " + rel.getType().toString());
		}
		
		for(String relId: watchList_DIFF.keySet())
		{
			int curr_NO_score = watchList_NO.get(relId);
			int curr_YES_score = watchList_YES.get(relId);
			logger.log(Level.INFO, "Leftover Relation " + relId +  " YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(relId));
		}
	}
	
	private static void writeResults(int noCount)
	{
		double recall = noCount / (finalPredictedRelations.size() * 1.0);
		sb.append("Single," +
				"Reduced_Factors,"+
				curr_Iteration + "," +
				 "OpenCoss," +
				initialSet40 + "," +
				YES_Threshold + "," +
				NO_Threshold + "," +
				  num_R_Associations + "," + 
				  num_IR_Associations + "," + 
				  num_R_Aggregations + "," + 
				  num_IR_Aggregations + "," + 
				  num_R_Generalizations + "," + 
				  num_IR_Generalizations + "," + 
				  num_RelevantinInitialTraining + "," + 
				  num_IrRelevantinInitialTraining + "," + K_Value + "," + 
				  noCount + "," + 
				  finalPredictedRelations.size() + "," +
				  recall + "," +
				  LPGroups_Correct + "," +
				  LPGroups_InCorrect + "," +
				  System.lineSeparator());
	}
	
	
	}
