package lu.svv.chetan.models.continuousLearning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

public class Pick_InitialTrainingSet {
	
	public static void main(String [] args)
	{
		List<String> set_Ids = new ArrayList<String>();
		set_Ids.add("R1");
		set_Ids.add("R2");
		set_Ids.add("R3");
		set_Ids.add("R4");
		set_Ids.add("R5");
		set_Ids.add("R6");
		set_Ids.add("R7");
		//randomizeOrder(set_Ids);
	}

	/*
	 * We want to pick the initial training set.
	 * The idea is to pick the training set in a way that the initial training set is representative of the distribution of relation types (ASS, GEN, AGG) in the actual training set.
	 * The minimum set to GEN is 10%. 
	 */
	public static void optimalOrder_byDistributionofRelationsType_SES(String filename)
	{		
		//Distribution of Relations
		int num_Generalizations = 5;
		int num_Aggregations = 8;
		int num_Attributes = 2;
		int total_Relations = Data.map_AllRelations.size();
		int totalRelations_TrainingSet = (int) (0.25 * total_Relations);
		int num_Associatons = totalRelations_TrainingSet - (num_Aggregations + num_Attributes + num_Generalizations);
		
		String [] order = ReadOrderedSet.readOrderedReqIdsFile(filename);
		List<String> relationIDs = Arrays.asList(order);
		
				for(String rel_ID: relationIDs)
				{
					Relation rel = Data.map_AllRelations.get(rel_ID);
					if(rel.getType().equals(RelationType.ASSOCIATION) && num_Associatons > 0)
					{
						Data.map_TrainingRelations.put(rel_ID, rel);
						num_Associatons--;
					}
					else if(rel.getType().equals(RelationType.AGGREGATION) && num_Aggregations > 0)
					{
						Data.map_TrainingRelations.put(rel_ID, rel);
						num_Aggregations--;
					}
					else if(rel.getType().equals(RelationType.GENERALIZATION) && num_Generalizations > 0)
					{
						Data.map_TrainingRelations.put(rel_ID, rel);
						num_Generalizations--;
					}
					else if(rel.getType().equals(RelationType.ATTRIBUTE) && num_Attributes > 0)
					{
						Data.map_TrainingRelations.put(rel_ID, rel);
						num_Attributes--;
					}
					else
					{
						Data.map_TestRelations.put(rel_ID, rel);
					}					
				}
	}
	
	public static void optimalOrder_byReqs(String filename)
	{
		String [] order = ReadOrderedSet.readOrderedReqIdsFile(filename);
		List<String> reqIDs = Arrays.asList(order); 
		int total_Relations = Data.map_AllRelations.size();
		int N90_percent_total = (int) (0.5 * total_Relations);
		int curr_size = 0;
		
			for(String reqID: reqIDs)
			{
				List<Relation> relations = Data.map_reqIDs_Relations.get(reqID);
				for(Relation rel: relations)
				{
					Data.map_TrainingRelations.put(rel.getRelation_Id(), rel);
					curr_size++;
					if(curr_size == N90_percent_total)
						break;
				}
				if(curr_size == N90_percent_total)
					break;
			}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}
	
	/*
	 * Randomly pick a relation from each requirement
	 */
	public static void diversifiedOrder_byDistributionofRelationsTypes_SES(int rndm)
	{		
		//Distribution of Relations
				int num_Generalizations = 11;
				int num_Aggregations = 17;
				int num_Attributes = 2;
				int total_Relations = Data.map_AllRelations.size();				
				int totalRelations_TrainingSet = (int) (0.4 * total_Relations);
				int num_Associatons = totalRelations_TrainingSet - (num_Aggregations + num_Attributes + num_Generalizations);
				
				pickInitialSet(rndm, num_Generalizations, num_Aggregations, num_Associatons, num_Attributes, totalRelations_TrainingSet, total_Relations);
	}

	/*
	 * Randomly pick a relation from each requirement
	 */
	public static void diversifiedOrder_byDistributionofRelationsTypes_IEE(int rndm)
	{		
		//Distribution of Relations
				int num_Generalizations = 8;
				int num_Aggregations = 11;
				int num_Attributes = 0;
				int total_Relations = Data.map_AllRelations.size();				
				int totalRelations_TrainingSet = (int) (0.4 * total_Relations);
				int num_Associatons = totalRelations_TrainingSet - (num_Aggregations + num_Attributes + num_Generalizations);
				
				pickInitialSet(rndm, num_Generalizations, num_Aggregations, num_Associatons, num_Attributes, totalRelations_TrainingSet, total_Relations);
	}
	
	/*
	 * Randomly pick a relation from each requirement
	 */
	public static void diversifiedOrder_byDistributionofRelationsTypes(int rndm, int num_Agg, int num_Gen, int num_Att, double initialSet_Ratio)
	{		
		//Distribution of Relations
				int num_Generalizations = num_Gen;
				int num_Aggregations = num_Agg;
				int num_Attributes = num_Att;
				int total_Relations = Data.map_AllRelations.size();				
				int totalRelations_TrainingSet = (int) (initialSet_Ratio * total_Relations);
				int num_Associatons = totalRelations_TrainingSet - (num_Aggregations + num_Attributes + num_Generalizations) + 1;
				
				pickInitialSet(rndm, num_Generalizations, num_Aggregations, num_Associatons, num_Attributes, totalRelations_TrainingSet, total_Relations);
	}
	
private static void pickInitialSet(int rndm, int num_Generalizations, int num_Aggregations, int num_Associatons, int num_Attributes, int totalRelations_TrainingSet, int total_Relations)
{
	List<String> reqIDs = new ArrayList<>(Data.map_reqIDs_Relations.keySet());
	
for(String reqID: reqIDs)
{
	List<Relation> relations = new ArrayList<>(); 
	relations.addAll(Data.map_reqIDs_Relations.get(reqID));
	int num_Relation_this_Req = relations.size();
	int relations_toPick = (int) (0.25 * num_Relation_this_Req);
	Random random = new Random(rndm);
	Collections.shuffle(relations, random);
	
	if(relations_toPick == 0)
	{
		relations_toPick = 1;
	}
		
	for(int i=0; i<relations_toPick; i++)
	{
		Relation rel = relations.get(i);
		String rel_ID = rel.getRelation_Id();
		if(rel.getType().equals(RelationType.ASSOCIATION) && num_Associatons > 0)
		{
			Data.map_TrainingRelations.put(rel_ID, rel);
			num_Associatons--;
		}
		else if(rel.getType().equals(RelationType.AGGREGATION) && num_Aggregations > 0)
		{
			Data.map_TrainingRelations.put(rel_ID, rel);
			num_Aggregations--;
		}
		else if(rel.getType().equals(RelationType.GENERALIZATION) && num_Generalizations > 0)
		{
			Data.map_TrainingRelations.put(rel_ID, rel);
			num_Generalizations--;
		}
		else if(rel.getType().equals(RelationType.ATTRIBUTE) && num_Attributes > 0)
		{
			Data.map_TrainingRelations.put(rel_ID, rel);
			num_Attributes--;
		}
	}				
}

LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
totalRelations_TrainingSet -= Data.map_TrainingRelations.size(); 
while(totalRelations_TrainingSet > 0)
{
for(String rel_ID: Completemap.keySet())
{
	Relation rel = Completemap.get(rel_ID);

	if(rel.getType().equals(RelationType.ASSOCIATION) && num_Associatons > 0)
	{
		Data.map_TrainingRelations.put(rel_ID, rel);
		num_Associatons--;
		totalRelations_TrainingSet--;
	}
	else if(rel.getType().equals(RelationType.AGGREGATION) && num_Aggregations > 0)
	{
		Data.map_TrainingRelations.put(rel_ID, rel);
		num_Aggregations--;
		totalRelations_TrainingSet--;
	}
	else if(rel.getType().equals(RelationType.GENERALIZATION) && num_Generalizations > 0)
	{
		Data.map_TrainingRelations.put(rel_ID, rel);
		num_Generalizations--;
		totalRelations_TrainingSet--;
	}
	else if(rel.getType().equals(RelationType.ATTRIBUTE) && num_Attributes > 0)
	{
		Data.map_TrainingRelations.put(rel_ID, rel);
		num_Attributes--;
		totalRelations_TrainingSet--;
	}
}
}

Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
Data.map_TestRelations.putAll(Completemap);
}
	}
