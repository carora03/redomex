package lu.svv.chetan.models.continuousLearning;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

public class R_Java_Interface {

    public static RList callRInterface() throws RserveException, REXPMismatchException, FileNotFoundException, IOException {
    	RList rl = new RList();
        RConnection c = new RConnection();
        if(c.isConnected()) {
            System.out.println("Connected to RServe.");
            if(c.needLogin()) {
                System.out.println("Providing Login");
                c.login("username", "password");
            }

            System.out.println("Reading script...");
            //c.eval("source(\"/Users/chetan.arora/Dropbox/PhD Folder/PhD_Papers/ESEC2017_RCode/R_CA_wd/JavaInterface.R\")");
            //c.eval("source(\"/Users/chetan.arora/Dropbox/PhD Folder/PhD_Papers/ESEC2017_RCode/R_CA_wd/R_Files/JavaInterface_2Learners_BVA.R\")");
            //c.eval("source(\"/Users/chetan.arora/Dropbox/PhD Folder/PhD_Papers/ESEC2017_RCode/R_CA_wd/R_Files/JavaInterface_2Learners_ReducedFactors_BVA.R\")");
            //c.eval("source(\"/Users/chetan.arora/Dropbox/PhD Folder/PhD_Papers/ESEC2017_RCode/R_CA_wd/R_Files/Experiment_RF.R\")");
            c.eval("source(\"/Users/chetan.arora/Dropbox/PhD Folder/PhD_Papers/ESEC2017_RCode/R_CA_wd/R_Files/JavaInterface_SingleLearner_ReducedFactors_BVA.R\")");
            //c.eval("source(\"/Users/chetan.arora/Dropbox/PhD Folder/PhD_Papers/ESEC2017_RCode/R_CA_wd/R_Files/JavaInterface_SingleLearner_ALLFactors_BVA.R\")");
            //c.eval("source(\"/Users/chetan.arora/Dropbox/PhD Folder/PhD_Papers/ESEC2017_RCode/R_CA_wd/R_Files/EnsembleLearning.R\")");
            rl = c.eval("rf_results").asList();
            System.out.println(rl.size());

        } else {
            System.out.println("Rserve could not connect");
        }

        c.close();
        System.out.println("Session Closed");
        return rl;
    }
    
    public static void initRInterface() throws RserveException, REXPMismatchException, FileNotFoundException, IOException {
        RConnection c = new RConnection();
        if(c.isConnected()) {
            System.out.println("Connected to RServe.");
            if(c.needLogin()) {
                System.out.println("Providing Login");
                c.login("username", "password");
            }

            System.out.println("Reading script...");
            c.eval("library(Rserve)");
            c.eval("Rserve(args = \"--vanilla\")");

        } else {
            System.out.println("Rserve could not connect");
        }

        c.close();
        System.out.println("Session Closed");
    }
}