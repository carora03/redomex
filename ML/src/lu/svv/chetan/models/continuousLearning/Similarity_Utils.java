package lu.svv.chetan.models.continuousLearning;

import java.util.List;

import simmetrics.similaritymetrics.Levenshtein;

public class Similarity_Utils {
	
	public static void main(String[] args)
	{
		System.out.println(doStringsfuzzyContainedIn("GSTSIM Projects Adress", "provide function to upload the GSTSIM project address"));
		
		
	}
	
	public static float getSimilarityScore(String s1, String s2)
	{
		Levenshtein lev = new Levenshtein();
		return lev.getSimilarity(s1, s2);
	}
	
	/*
	 * This assumes that a 
	 */
	public static String doStringsfuzzyContainedIn(String s1, String s2)
	{
		String[] s1tokens = Utilities.Tokenize(s1.toLowerCase());
		String[] s2tokens = Utilities.Tokenize(s2.toLowerCase());
		
		if(s1tokens.length > s2tokens.length)
			return "";
		
		int matchingtokens = 0;
		StringBuilder sb = new StringBuilder();
		
		for(int i=0; i<s1tokens.length; i++)
		{
			for(int j=i; j<s2tokens.length; j++)
			{
				if(getSimilarityScore(s1tokens[i], s2tokens[j]) > 0.8)
				{
					matchingtokens++;
					sb.append(s2tokens[j] + " ");
					break;
				}
			}
		}
		
		if(matchingtokens == s1tokens.length)
			return sb.toString().trim();
		
		return "";
	}
	
	public static String doStringsfuzzyMatch_MorePrecise(String s1, String s2)
	{
		if(getSimilarityScore(s1, s2) < 0.7)
		{
			return "";
		}
		
		String[] s1tokens = Utilities.Tokenize(s1.toLowerCase());
		String[] s2tokens = Utilities.Tokenize(s2.toLowerCase());
				
		
		int matchingtokens = 0;
		StringBuilder sb = new StringBuilder();
		
		for(int i=0; i<s1tokens.length; i++)
		{
			for(int j=i; j<s2tokens.length; j++)
			{
				if(getSimilarityScore(s1tokens[i], s2tokens[j]) > 0.749)
				{
					matchingtokens++;
					sb.append(s2tokens[j] + " ");
					break;
				}
			}
		}
		
		if(matchingtokens == s1tokens.length)
			return sb.toString().trim();
		
		return "";
	}
	
	public static boolean doListofStringsfuzzyMatch_MorePrecise(List<String> lst1, List<String> lst2)
	{
		if(lst1.size() != lst2.size())
			return false;
		
		if(lst1.size() == 0 || lst2.size() == 0)
			return false;
		
		int matchingStrings_fwd = 0;
		int matchingStrings_rev = 0;
		
		for(String s1: lst1)
		{
			for(String s2: lst2)
			{
				if(!doStringsfuzzyMatch_MorePrecise(s1, s2).isEmpty())
				{
					matchingStrings_fwd++;
					break;
				}
			}
		}
		
		for(String s2: lst2)
		{
			for(String s1: lst1)
			{
				if(!doStringsfuzzyMatch_MorePrecise(s2, s1).isEmpty())
				{
					matchingStrings_rev++;
					break;
				}
			}
		}
		
		if((matchingStrings_fwd == matchingStrings_rev) && (matchingStrings_rev  == lst1.size()))
			return true;
		
		return false;
	}
	
	public static boolean is_lst1_a_Subsetof_lst2(List<String> lst1, List<String> lst2)
	{
		if(lst1.size() > lst2.size())
			return false;
		
		if(lst1.size() == 0 || lst2.size() == 0)
			return false;
		
		int matchingStrings_fwd = 0;
		for(String s1: lst1)
		{
			for(String s2: lst2)
			{
				if(!doStringsfuzzyMatch_MorePrecise(s1, s2).isEmpty())
				{
					matchingStrings_fwd++;
					break;
				}
			}
		}
		
		if((matchingStrings_fwd == lst1.size()))
			return true;
		
		return false;
	}

}
