package lu.svv.chetan.models.continuousLearning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;


public class RandomizeOrder {
	
	public static void main(String [] args)
	{
		List<String> set_Ids = new ArrayList<String>();
		set_Ids.add("R1");
		set_Ids.add("R2");
		set_Ids.add("R3");
		set_Ids.add("R4");
		set_Ids.add("R5");
		set_Ids.add("R6");
		set_Ids.add("R7");
		//randomizeOrder(set_Ids);
	}
	
	public static void randomizeOrder_byRelations(int seed)
	{		
		List<String> relationIDs = new ArrayList<>(Data.map_AllRelations.keySet());
		Random random = new Random(seed);
		Collections.shuffle(relationIDs, random);
		int total_Relations = Data.map_AllRelations.size();
		int N90_percent_total = (int) (0.25 * total_Relations);
		int curr_size = 0;
		
			
				for(String rel_ID: relationIDs)
				{
					Relation rel = Data.map_AllRelations.get(rel_ID);
					Data.map_TrainingRelations.put(rel_ID, rel);
					curr_size++;
					if(curr_size == N90_percent_total)
						break;
				}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}


	
	public static void randomizeOrder_byReqs()
	{		
		List<String> reqIDs = new ArrayList<>(Data.map_reqIDs_Relations.keySet());
		Collections.shuffle(reqIDs);
		int total_Relations = Data.map_AllRelations.size();
		int N90_percent_total = (int) (0.5 * total_Relations);
		int curr_size = 0;
		
			for(String reqID: reqIDs)
			{
				List<Relation> relations = Data.map_reqIDs_Relations.get(reqID);
				for(Relation rel: relations)
				{
					Data.map_TrainingRelations.put(rel.getRelation_Id(), rel);
					curr_size++;
					if(curr_size == N90_percent_total)
						break;
				}
				if(curr_size == N90_percent_total)
					break;
			}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}
	
	public static void optimalOrder_byRelations(String filename)
	{		
		String [] order = ReadOrderedSet.readOrderedReqIdsFile(filename);
		List<String> relationIDs = Arrays.asList(order); 
		int total_Relations = Data.map_AllRelations.size();
		int N90_percent_total = (int) (0.25 * total_Relations);
		int curr_size = 0;
		
			
				for(String rel_ID: relationIDs)
				{
					Relation rel = Data.map_AllRelations.get(rel_ID);										
					if(curr_size > N90_percent_total)
					{
						Data.map_TestRelations.put(rel_ID, rel);
					}
					else
					{
						Data.map_TrainingRelations.put(rel_ID, rel);
					}
					curr_size++;
				}
	}
	
	public static void optimalOrder_byReqs(String filename)
	{
		String [] order = ReadOrderedSet.readOrderedReqIdsFile(filename);
		List<String> reqIDs = Arrays.asList(order); 
		int total_Relations = Data.map_AllRelations.size();
		int N90_percent_total = (int) (0.5 * total_Relations);
		int curr_size = 0;
		
			for(String reqID: reqIDs)
			{
				List<Relation> relations = Data.map_reqIDs_Relations.get(reqID);
				for(Relation rel: relations)
				{
					Data.map_TrainingRelations.put(rel.getRelation_Id(), rel);
					curr_size++;
					if(curr_size == N90_percent_total)
						break;
				}
				if(curr_size == N90_percent_total)
					break;
			}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}
	
	/*
	 * Randomly pick a relation from each requirement
	 */
	public static void diversifiedOrder_byRelations(int rndm)
	{		
		List<String> reqIDs = new ArrayList<>(Data.map_reqIDs_Relations.keySet());
		
			for(String reqID: reqIDs)
			{
				List<Relation> relations = new ArrayList<>(); 
				relations.addAll(Data.map_reqIDs_Relations.get(reqID));
				Random random = new Random(rndm);
				Collections.shuffle(relations, random);
				Data.map_TrainingRelations.put(relations.get(0).getRelation_Id(), relations.get(0));				
			}
		
		LinkedHashMap<String, Relation> Completemap = new LinkedHashMap<>(Data.map_AllRelations);
		Completemap.keySet().removeAll(Data.map_TrainingRelations.keySet());
		Data.map_TestRelations.putAll(Completemap);
	}
	
}
