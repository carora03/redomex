package lu.svv.chetan.models.continuousLearning;

public enum RelevanceType {
	YES("YES"),
	NO("NO");
	
	private final String name;       

	 private RelevanceType(String s) {
	        name = s;
}
	 
	 public boolean equalsName(String otherName) {
	        return (otherName == null) ? false : name.equals(otherName);
	    }

	    public String toString() {
	       return this.name;
	    }
}
