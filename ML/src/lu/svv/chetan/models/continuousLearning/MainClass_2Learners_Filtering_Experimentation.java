package lu.svv.chetan.models.continuousLearning;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RserveException;

import gate.Corpus;
/*
 * This class is like a recommendation system, you don't filter out anything.
 * But recommend if something is NO
 */
public class MainClass_2Learners_Filtering_Experimentation {
	
	//Fixed variables
	static String wd = "/Users/chetan.arora/Dropbox/PhD Folder/work/Experimentation/Model_Extraction_ML/";
	static String Relations_FileName = "OpenCoss_Interview_Data_learning";
	static String directory = new String();
	static Logger logger = Logger.getLogger(MainClass_2Learners_Filtering_Experimentation.class.getName());
	
	//Algorithm specific variables
	static int number_RelationsperRound = 1; //Number of relations to be added to the training set (show to the user) per round, by default
	static int num_minRoundsBeforeDecision = 0; //Num of min rounds before adding NO relations directly to the training set, without confirming		
	static int minScoreforDecision = 5; //Min score for making a NO recommendation
	static StringBuilder sb = new StringBuilder();
	static StringBuilder log_sb = new StringBuilder(); 
	static double YES_Threshold = 0.65;
	static double NO_Threshold = 0.0;	
	
	//Per Algorithm Run variables
	static LinkedHashMap<String, Integer> watchList_NO = new LinkedHashMap<>();
	static LinkedHashMap<String, Integer> watchList_YES = new LinkedHashMap<>();
	static LinkedHashMap<String, Integer> watchList_DIFF = new LinkedHashMap<>();
	public static int curr_Iteration = 0; //The number of iterations of the whole algo
	static List<String> finalPredictedRelations = new ArrayList<>();
	
	static int correctSoFar_NO = 0;
	static int num_RelevantinInitialTraining = 0;
	static int num_IrRelevantinInitialTraining = 0;
	
	static int num_R_Associations = 0;
	static int num_IR_Associations = 0;
	static int num_R_Aggregations = 0;
	static int num_IR_Aggregations = 0;
	static int num_R_Generalizations = 0;
	static int num_IR_Generalizations = 0;
	
	//Per iteration variables 
	static int roundNumber = 0; //Current round number
	static int maxPossibleRounds = 0;
	
	static double initialSet40 = 0.4;
	
	public static void main(String [] args) throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		long startTime = System.currentTimeMillis();
		//R_Java_Interface.initRInterface();
		init();
		double [] NO_Thresholds = {0.65, 0.7, 0.75, 0.8, 0.85};
		
		for(double NO: NO_Thresholds)
		{
			NO_Threshold = NO;
			//Write thresholds data to a file, so that R can read it
			WriteData.writeSimpleData(YES_Threshold + System.lineSeparator() + NO_Threshold, wd + "Thresholds.csv", false);
			
			executeExperimentSet();
		}
		System.out.println((System.currentTimeMillis() - startTime)/1000 + " seconds");
	}
	
	private static void executeExperimentSet() throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		for(int i = 1; i<=10; i++)
		{
			curr_Iteration = i;
			
		//Step 1 - Initialize the first 25% data, by picking a relation per requirement		
		
		Pick_InitialTrainingSet.diversifiedOrder_byDistributionofRelationsTypes(curr_Iteration, InitalSet_Numbers.num_Aggregation_OpenCoss_initialSet40, 
																										 InitalSet_Numbers.num_Generalization_OpenCoss_initialSet40,
																										 InitalSet_Numbers.num_Attributes_OpenCoss,
																										 initialSet40);
			
		countNumRelevantRelationsinInitialSet();
		maxPossibleRounds = Data.map_TestRelations.size();
		
		log_sb.append(Utilities.listToString((Collection<String>)Data.map_TrainingRelations.keySet(), " - "));
		log_sb.append(System.lineSeparator());
		log_sb.append("*** Round " + curr_Iteration + "_" + roundNumber + " ***" + System.lineSeparator());
		//Initialize the watchlists
				for(String key: Data.map_TestRelations.keySet())
				{
					watchList_NO.put(key, 0);
					watchList_YES.put(key, 0);
					watchList_DIFF.put(key, 0);
				}
		singleIteration(); //First Iteration

		//Subsequent Iterations
		while((watchList_DIFF.size() > (0.01 * Data.map_AllRelations.size())) && (Data.map_TrainingRelations.size() < Data.map_AllRelations.size()))
		{	
			log_sb.append("*** Round " + curr_Iteration + "_" + roundNumber + " ***" + System.lineSeparator());
			Data.reset();						
			pickNextCandidatesforTraining();
			singleIteration();			
			logger.log(Level.INFO, "***Round " + roundNumber + " completed with " + watchList_DIFF.size() + " in the watchlist and number of elements in automatic NO recommendation =" + finalPredictedRelations.size() + "and correct = " + correctSoFar_NO);	
			roundNumber++;
		}
		
		//Post-Mortem (1 Algorithm Run Finished) 
			//Utilities.printList(finalPredictedRelations);
			computeAccuracy();
			WriteData.writeSimpleData(sb.toString(), directory + "AAccuracy_continuousLearning.csv", true);
			resetAlgorithmRunVariables();
		}
	//executeDataCollection(1);
		WriteData.writeSimpleData(log_sb.toString(), directory + "AA_LOG.csv", false);
	}
	
	private static void countNumRelevantRelationsinInitialSet()
	{
		int num_IrRelevant = 0;
		for(String key: Data.map_TrainingRelations.keySet())
		{
			Relation rel = Data.map_TrainingRelations.get(key);
			if(rel.getIsRelevant().equals(RelevanceType.NO))
			{
				num_IrRelevant++;
			}
			
			if(rel.getType().equals(RelationType.ASSOCIATION))
			{
				if(rel.getIsRelevant().equals(RelevanceType.NO))
				{
					num_IR_Associations++;
				}
				else
				{
					num_R_Associations++;
				}
				
			}
			else if(rel.getType().equals(RelationType.AGGREGATION))
			{
				if(rel.getIsRelevant().equals(RelevanceType.NO))
				{
					num_IR_Aggregations++;
				}
				else
				{
					num_R_Aggregations++;
				}				
			}
			else if(rel.getType().equals(RelationType.GENERALIZATION))
			{
				if(rel.getIsRelevant().equals(RelevanceType.NO))
				{
					num_IR_Generalizations++;
				}
				else
				{
					num_R_Generalizations++;
				}				
			}
		}
		num_IrRelevantinInitialTraining = num_IrRelevant;
		num_RelevantinInitialTraining = Data.map_TrainingRelations.size() - num_IrRelevant;
		log_sb.append("Total Relations selected in initial Training Set = " + Data.map_TrainingRelations.size() + System.lineSeparator());
	
		log_sb.append("Total Aggregations selected in initial Training Set = " + num_R_Aggregations + num_IR_Aggregations  + System.lineSeparator());
		log_sb.append("Aggregations Split = " + num_R_Aggregations + " R / IR " + num_IR_Aggregations + System.lineSeparator());
		
		log_sb.append("Total Generalizations selected in initial Training Set = " + num_R_Generalizations + num_IR_Generalizations + System.lineSeparator());
		log_sb.append("Generalizations Split = " + num_R_Generalizations + " R / IR " + num_IR_Generalizations + System.lineSeparator());
		
		log_sb.append("Total Associations selected in initial Training Set = " + num_R_Associations + num_IR_Associations + System.lineSeparator());
		log_sb.append("Associations Split = " + num_R_Associations + " R / IR " + num_IR_Associations + System.lineSeparator());
	}
	
	private static void singleIteration() throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		FillDependentRulesData.FillDependentRulesData_TrainingData();
		FillDependentRulesData.FillDependentRulesData_TestData();	
		
		String str_trainingFile = directory + Relations_FileName + "_TrainingSet.csv";
		String str_testFile = directory + Relations_FileName + "_TestSet.csv";
		
		WriteData.writeCSV(Data.map_TrainingRelations, wd + Relations_FileName + "_TrainingSet_0.csv", str_trainingFile);
		WriteData.writeCSV(Data.map_TestRelations, wd + Relations_FileName + "_TestSet_0.csv", str_testFile);
		
		WriteData.writeSimpleData(str_trainingFile + System.lineSeparator() + str_testFile, wd + "filename.csv", false);
		
		if(Data.map_TestRelations.size() > 0)
		{
			RList rl = R_Java_Interface.callRInterface();
			//Call the R Code
			String [] vector_MatchingNO = getMatchingRelationsFromRList(rl, 1, 4, "NO");
			String [] vector_MatchingYES = getMatchingRelationsFromRList(rl, 1, 4, "YES");
			updateWatchlist(vector_MatchingNO, vector_MatchingYES);
		}
		
	}
	
	
	private static void init()  throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		//Create a directory to store all the files for this run
		long yourmilliseconds = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy # HHmm");    
		Date resultdate = new Date(yourmilliseconds);		
		File f = new File(wd + "Executions/" + "Experiment_Filtering_" + "OpenCoss_" + initialSet40);
		f.mkdir();
		directory = f.getAbsolutePath() + "/";
		System.out.println(directory);
		
		/*
		 * Read Requirements Text
		 */
			String Requirements_FileName = "OpenCoss_Data_Requirements";
			ReadCSV.readReqsTextFile("./res/req_docs/" + Requirements_FileName + ".csv");
			
			/*
			 * Process Requirements Text and find the NPs and VBs in each requirement
			 */
				try {
					Corpus corpus = GATE_NLP.init();
					System.out.println(corpus.getDocumentName(0));
					GATE_NLP.extractData(corpus);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				/*
				 * Read Relations Text
				 */								
					ReadCSV.readRelationsFile("./res/relation_docs/" + Relations_FileName + ".csv");
							
					/*
					 * Get Phrases for the association relations labels
					 * 
					 * ***Public variables updated**
					 * 		Data.map_relationIDs_VBs
					 * 		Data.map_relationIDs_NPs
					 */
					for(String reqID: Data.map_reqIDs_Relations.keySet())
					{
						List<Relation> list_Relations = Data.map_reqIDs_Relations.get(reqID);
						List<String> list_VBs = Data.map_reqIDs_VBs.get(reqID);
						List<String> list_NPs = Data.map_reqIDs_NPs.get(reqID);
						for(Relation rel: list_Relations)
						{
							if(!rel.getRelation_Text().trim().equals(""))
							{
								List<String> lst_VBs = DependentRules_Utils.getLabelPhrases(rel.getRelation_Text().trim(), list_VBs);
								List<String> lst_NPs = DependentRules_Utils.getLabelPhrases(rel.getRelation_Text().trim(), list_NPs);
								if(lst_VBs.size() > 0)
									Data.map_relationIDs_VBs.put(rel.getRelation_Id(), lst_VBs);
								if(lst_NPs.size() > 0)
								Data.map_relationIDs_NPs.put(rel.getRelation_Id(), lst_NPs);
							}
						}				 
					}					
	}
	
	private static void resetAlgorithmRunVariables()
	{
		watchList_NO = new LinkedHashMap<>();
		watchList_YES = new LinkedHashMap<>();
		watchList_DIFF = new LinkedHashMap<>();		
		roundNumber = 0; //Current round number
		
		finalPredictedRelations = new ArrayList<>();
		Data.reset();
		Data.map_TrainingRelations = new LinkedHashMap<>();
		Data.map_TestRelations = new LinkedHashMap<>();
		for(String rel_Id: Data.map_AllRelations.keySet())
		{
			Relation rel = Data.map_AllRelations.get(rel_Id);
			rel.setIsRelevant(Data.map_relationIDs_Relevance.get(rel_Id));
		}
		sb = new StringBuilder();
		
		correctSoFar_NO = 0;
		num_IrRelevantinInitialTraining = 0;
		num_RelevantinInitialTraining = 0;
		num_R_Associations = 0;
		num_IR_Associations = 0;
		num_R_Aggregations = 0;
		num_IR_Aggregations = 0;
		num_R_Generalizations = 0;
		num_IR_Generalizations = 0;
	}
		
	private static void pickNextCandidatesforTraining()
	{
		int i = 0;
		int automaticRelationsAddedSoFar = 0;
		boolean didWeAddAnythingAutomatically = false;
		Iterator<String> it_keys = watchList_DIFF.keySet().iterator();
		
		/*
		 * Strength of Evidence
		 */
		double a = (maxPossibleRounds - (roundNumber * number_RelationsperRound))/5;
		minScoreforDecision = (int) Math.max(a, 5);
		
		/*
		 * The logic for adding relations directly to the training set (NOT shown to the user)
		 */
		if(roundNumber >= num_minRoundsBeforeDecision)
		{
			while(it_keys.hasNext())
			{
				String rel_Id = it_keys.next();
				int curr_diff_score = watchList_DIFF.get(rel_Id);
				int curr_NO_score = watchList_NO.get(rel_Id);
				int curr_YES_score = watchList_YES.get(rel_Id);
				if((curr_diff_score >= minScoreforDecision) && curr_NO_score > curr_YES_score)
				{
					logger.log(Level.INFO, "Automatically Added:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(rel_Id));
					log_sb.append("Automatically Added:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(rel_Id) + System.lineSeparator());
					Data.map_AllRelations.get(rel_Id).setIsRelevant(RelevanceType.NO);
					Data.map_TrainingRelations.put(rel_Id, Data.map_AllRelations.get(rel_Id));
					Data.map_TestRelations.remove(rel_Id);
					automaticRelationsAddedSoFar++;
					if(automaticRelationsAddedSoFar >= 1)
					{
						didWeAddAnythingAutomatically = true;
					}
						
					finalPredictedRelations.add(rel_Id);
						if(Data.map_relationIDs_Relevance.get(rel_Id).equals(RelevanceType.NO))
						{
							correctSoFar_NO++;
						}
					

					it_keys.remove();
				}
			}
		}
		
		/*
		 * Logic for showing relations to the user by default 
		 */
		if(didWeAddAnythingAutomatically == false)
		{
			it_keys = watchList_DIFF.keySet().iterator();
			while(it_keys.hasNext())
			{			
				String rel_Id = it_keys.next();
				int curr_diff_score = watchList_DIFF.get(rel_Id);
				int curr_NO_score = watchList_NO.get(rel_Id);
				int curr_YES_score = watchList_YES.get(rel_Id);
				Relation curr_Relation = Data.map_AllRelations.get(rel_Id);
				Data.map_TrainingRelations.put(rel_Id, curr_Relation);
				Data.map_TestRelations.remove(rel_Id);
				it_keys.remove();
				logger.log(Level.INFO, "Default Added:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(rel_Id));
				log_sb.append("Default Added:: " + rel_Id + System.lineSeparator());
				i++;
				
				if(curr_diff_score > 10 && curr_YES_score > 10)
				{
					System.out.println("Default Addition continued:: Relation " + rel_Id +  " added at YES :: " + curr_YES_score + " and NO :: " + curr_NO_score);
					continue;				
				}
				
				if(i == number_RelationsperRound)
				{
					break;
				}
			}	
		}
	}
	
	/*
	 * This is the function to sort a hashmap
	 */
	public static <K, V extends Comparable<? super V>> LinkedHashMap<K, V> sortByValue(LinkedHashMap<K, V> map) {
	    return map.entrySet()
	              .stream()
	              .sorted(HashMap.Entry.comparingByValue(/*Collections.reverseOrder()*/))
	              .collect(Collectors.toMap(
	            		  HashMap.Entry::getKey, 
	            		  HashMap.Entry::getValue, 
	                (e1, e2) -> e1, 
	                LinkedHashMap::new
	              ));
	}
	
		
	/*
	 * Update the YES and NO watchlists
	 */
	private static void updateWatchlist(String [] vector_MatchingNO, String [] vector_MatchingYES)
	{
		for(String str: vector_MatchingNO)
		{
			int prev_Score = watchList_NO.get(str);
			watchList_NO.put(str, prev_Score + 1);
			watchList_DIFF.put(str, Math.abs(watchList_NO.get(str) - watchList_YES.get(str)));
			//watchList_DIFF.put(str, watchList_NO.get(str) - watchList_YES.get(str));
		}
		
		for(String str: vector_MatchingYES)
		{
			int prev_Score = watchList_YES.get(str);
			watchList_YES.put(str, prev_Score + 1);
			watchList_DIFF.put(str, Math.abs(watchList_NO.get(str) - watchList_YES.get(str)));
			//watchList_DIFF.put(str, (watchList_NO.get(str) - watchList_YES.get(str)));
		}
		//Sort the difference watchlist in an ascending order
		watchList_DIFF = sortByValue(watchList_DIFF);
	}

	/*
	 * This fuctions returns the array of relations that match YES/NO probabilities predicted greater than a threshold
	 */
	private static String [] getMatchingRelationsFromRList(RList rlist, int index_relations, int index_prob, String result) throws REXPMismatchException
	{
		String[] vector_results = rlist.at(index_prob).asStrings(); // 4 0r 5		
		String[] vector_RelIDs = rlist.at(index_relations).asStrings(); //1
		
		int[] prob_indices = IntStream.range(0, vector_results.length)
                .filter(i -> vector_results[i].equals(result))
                .toArray();
		
		String [] filtered_Relations_prob = new String[prob_indices.length];
		
		int j = 0;
		for (int i : prob_indices) {
			filtered_Relations_prob[j] = vector_RelIDs[i];
			j++;
		}		
		return filtered_Relations_prob;
	}

	private static void computeAccuracy()
	{
		int noCount = 0;
		for(String relId: finalPredictedRelations)
		{
			if(Data.map_relationIDs_Relevance.get(relId).equals(RelevanceType.NO))
			{
				noCount++;
			}
		}
		logger.log(Level.INFO, "**** We got " + noCount + " NO relations right out of " + finalPredictedRelations.size() + " ****");
		
		writeResults(noCount);
		//sb.append(curr_Iteration + "," + num_RelevantinInitialTraining + "," + num_IrRelevantinInitialTraining + ",NO," + noCount + "," + finalPredictedRelations.size() + System.lineSeparator());
		log_sb.append("**** We got " + noCount + " NO relations right out of " + finalPredictedRelations.size() + " ****");
		
		for(String relId: watchList_DIFF.keySet())
		{
			int curr_NO_score = watchList_NO.get(relId);
			int curr_YES_score = watchList_YES.get(relId);
			logger.log(Level.INFO, "Leftover Relation " + relId +  " YES :: " + curr_YES_score + " and NO :: " + curr_NO_score + " -- actual Relevance -- " + Data.map_relationIDs_Relevance.get(relId));
		}
	}
	
	private static void writeResults(int noCount)
	{
		sb.append("Reccommendation," +
				curr_Iteration + "," +
				 "OpenCoss," +
				initialSet40 + "," +
				YES_Threshold + "," +
				NO_Threshold + "," +
				  num_R_Associations + "," + 
				  num_IR_Associations + "," + 
				  num_R_Aggregations + "," + 
				  num_IR_Aggregations + "," + 
				  num_R_Generalizations + "," + 
				  num_IR_Generalizations + "," + 
				  num_RelevantinInitialTraining + "," + 
				  num_IrRelevantinInitialTraining + ",NO," + 
				  noCount + "," + 
				  finalPredictedRelations.size() + System.lineSeparator());
	}
	
	
	}
