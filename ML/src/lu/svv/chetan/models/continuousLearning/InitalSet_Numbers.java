package lu.svv.chetan.models.continuousLearning;

public class InitalSet_Numbers {
	
	static int num_Aggregation_OpenCoss_initialSet10 = 4;
	static int num_Aggregation_OpenCoss_initialSet20 = 8;
	static int num_Aggregation_OpenCoss_initialSet25 = 10;
	static int num_Aggregation_OpenCoss_initialSet30 = 12;
	static int num_Aggregation_OpenCoss_initialSet35 = 14;
	static int num_Aggregation_OpenCoss_initialSet40 = 16;
	static int num_Aggregation_OpenCoss_initialSet50 = 18;
	
	static int num_Generalization_OpenCoss_initialSet10 = 2;
	static int num_Generalization_OpenCoss_initialSet20 = 4;	
	static int num_Generalization_OpenCoss_initialSet25 = 4;
	static int num_Generalization_OpenCoss_initialSet30 = 5;
	static int num_Generalization_OpenCoss_initialSet35 = 6;
	static int num_Generalization_OpenCoss_initialSet40 = 7;
	static int num_Generalization_OpenCoss_initialSet50 = 8;
	
	static int num_Attributes_OpenCoss = 4;	
	
	static int num_Aggregation_SES_initialSet10 = 5;
	static int num_Aggregation_SES_initialSet20 = 10;
	static int num_Aggregation_SES_initialSet25 = 12;
	static int num_Aggregation_SES_initialSet30 = 15;
	static int num_Aggregation_SES_initialSet35 = 18;
	static int num_Aggregation_SES_initialSet40 = 20;
	static int num_Aggregation_SES_initialSet50 = 22;
	
	static int num_Generalization_SES_initialSet10 = 4;
	static int num_Generalization_SES_initialSet20 = 7;
	static int num_Generalization_SES_initialSet25 = 8;
	static int num_Generalization_SES_initialSet30 = 9;
	static int num_Generalization_SES_initialSet35 = 10;
	static int num_Generalization_SES_initialSet40 = 12;
	static int num_Generalization_SES_initialSet50 = 14;
	
	static int num_Attributes_SES = 2;

	static int num_Aggregation_IEE_initialSet10 = 2;
	static int num_Aggregation_IEE_initialSet20 = 4;
	static int num_Aggregation_IEE_initialSet25 = 5;
	static int num_Aggregation_IEE_initialSet30 = 6;
	static int num_Aggregation_IEE_initialSet35 = 7;
	static int num_Aggregation_IEE_initialSet40 = 8;
	static int num_Aggregation_IEE_initialSet50 = 9;
	
	static int num_Generalization_IEE_initialSet10 = 4;
	static int num_Generalization_IEE_initialSet20 = 7;
	static int num_Generalization_IEE_initialSet25 = 8;
	static int num_Generalization_IEE_initialSet30 = 9;
	static int num_Generalization_IEE_initialSet35 = 10;
	static int num_Generalization_IEE_initialSet40 = 11;
	static int num_Generalization_IEE_initialSet50 = 13;
	
	static int num_Attributes_IEE = 0;
}
