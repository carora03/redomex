package lu.svv.chetan.models.continuousLearning;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;


public class Data {
	
	public static LinkedHashMap<String, String> map_reqIDs_Text = new LinkedHashMap<>();
	
	public static LinkedHashMap<String, List<String>> map_reqIDs_NPs = new LinkedHashMap<>();
	public static LinkedHashMap<String, List<String>> map_reqIDs_VBs = new LinkedHashMap<>();
	public static LinkedHashMap<String, List<Relation>> map_reqIDs_Relations = new LinkedHashMap<>();
	public static       HashMap<String, List<List<String>>> map_LPgroups = new HashMap<>();
	
	public static LinkedHashMap<String, List<String>> map_relationIDs_NPs = new LinkedHashMap<>();
	public static LinkedHashMap<String, List<String>> map_relationIDs_VBs = new LinkedHashMap<>();
	public static LinkedHashMap<String, RelevanceType> map_relationIDs_Relevance = new LinkedHashMap<>();
	
	public static LinkedHashMap<String, Relation> map_AllRelations = new LinkedHashMap<String, Relation>();
	public static LinkedHashMap<String, Relation> map_TrainingRelations = new LinkedHashMap<String, Relation>();
	public static LinkedHashMap<String, Relation> map_TestRelations = new LinkedHashMap<String, Relation>();
	
	public static String[] list_Conjunctions = {"or", "and"};
	
	public static LinkedHashMap<String, HashSet<String>> map_relationIDs_matchingEndConcepts = new LinkedHashMap<>();
	public static LinkedHashMap<String, HashSet<String>> map_relationIDs_matchingVerbs = new LinkedHashMap<>();
	public static LinkedHashMap<String, HashSet<String>> map_relationIDs_matchingRelationNPs = new LinkedHashMap<>();
	public static LinkedHashMap<String, HashSet<String>> map_relationIDs_matchingSourcetoAny = new LinkedHashMap<>();
	public static LinkedHashMap<String, HashSet<String>> map_relationIDs_matchingTargettoAny = new LinkedHashMap<>();
	public static LinkedHashMap<String, HashSet<String>> map_relationIDs_subsetRelations = new LinkedHashMap<>();
	public static LinkedHashMap<String, HashSet<String>> map_relationIDs_supersetRelations = new LinkedHashMap<>();
	
	public static LinkedHashMap<String, HashSet<String>> map_relationIDs_sameReqRelations = new LinkedHashMap<>();
	public static LinkedHashMap<String, HashSet<String>> map_relationIDs_sameReqsameRuleRelations = new LinkedHashMap<>();
	
	
	public static void reset()
	{
		for(String key: map_AllRelations.keySet())
		{
			map_AllRelations.get(key).resetDependentInfo();
		}
	}
}
