package lu.svv.chetan.models.continuousLearning;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RserveException;

import gate.Corpus;

public class ComputeInitialSetNumbers {
	
	static int num_RelevantinInitialTraining = 0;
	static int num_IrRelevantinInitialTraining = 0;
	static StringBuilder sb = new StringBuilder();
	static String Relations_FileName = "SES_Interview_Data_learning";
	
	public static void main(String [] args) throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		init();
		for(int i = 0; i<101; i++)
		{
			RandomizeOrder.diversifiedOrder_byRelations(i);
			countNumRelevantRelationsinInitialSet();
			sb.append(i + "," + num_RelevantinInitialTraining + "," + num_IrRelevantinInitialTraining + System.lineSeparator());
			num_RelevantinInitialTraining = 0;
			num_IrRelevantinInitialTraining = 0;
			Data.reset();
			Data.map_TrainingRelations = new LinkedHashMap<>();
			Data.map_TestRelations = new LinkedHashMap<>();
		}
		WriteData.writeSimpleData(sb.toString(), "./Experiment_OneRelperReq_InitialNumbers.csv", false);
	}
	
	private static void countNumRelevantRelationsinInitialSet()
	{
		int num_IrRelevant = 0;
		for(String key: Data.map_TrainingRelations.keySet())
		{
			Relation rel = Data.map_TrainingRelations.get(key);
			if(rel.getIsRelevant().equals(RelevanceType.NO))
			{
				num_IrRelevant++;
			}
		}
		num_IrRelevantinInitialTraining = num_IrRelevant;
		num_RelevantinInitialTraining = Data.map_TrainingRelations.size() - num_IrRelevant;
	}


	private static void init()  throws RserveException, FileNotFoundException, REXPMismatchException, IOException
	{
		
		/*
		 * Read Requirements Text
		 */
			String Requirements_FileName = "SES_Data_Requirements";
			ReadCSV.readReqsTextFile("./res/req_docs/" + Requirements_FileName + ".csv");
			
			/*
			 * Process Requirements Text and find the NPs and VBs in each requirement
			 */
				try {
					Corpus corpus = GATE_NLP.init();
					System.out.println(corpus.getDocumentName(0));
					GATE_NLP.extractData(corpus);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				/*
				 * Read Relations Text
				 */								
					ReadCSV.readRelationsFile("./res/relation_docs/" + Relations_FileName + ".csv");
							
					/*
					 * Get Phrases for the association relations labels
					 * 
					 * ***Public variables updated**
					 * 		Data.map_relationIDs_VBs
					 * 		Data.map_relationIDs_NPs
					 */
					for(String reqID: Data.map_reqIDs_Relations.keySet())
					{
						List<Relation> list_Relations = Data.map_reqIDs_Relations.get(reqID);
						List<String> list_VBs = Data.map_reqIDs_VBs.get(reqID);
						List<String> list_NPs = Data.map_reqIDs_NPs.get(reqID);
						for(Relation rel: list_Relations)
						{
							if(!rel.getRelation_Text().trim().equals(""))
							{
								List<String> lst_VBs = DependentRules_Utils.getLabelPhrases(rel.getRelation_Text().trim(), list_VBs);
								List<String> lst_NPs = DependentRules_Utils.getLabelPhrases(rel.getRelation_Text().trim(), list_NPs);
								if(lst_VBs.size() > 0)
									Data.map_relationIDs_VBs.put(rel.getRelation_Id(), lst_VBs);
								if(lst_NPs.size() > 0)
								Data.map_relationIDs_NPs.put(rel.getRelation_Id(), lst_NPs);
							}
						}				 
					}					
	}
	
	
}
