package lu.svv.chetan.models;

import java.util.ArrayList;
import java.util.List;

public class DependentRules_Utils {
	
	
	public static void main(String [] args)
	{
		List<String> list = new ArrayList<>();
		list.add("GSTSTIM");
		list.add("GSTSIM Project Address");
		list.add("Satellite Communication");
		list.add("function");
		
		Utilities.printList(getLabelPhrases("provide function to store GSTSIM Project Address", list));
	}
	
	public static List<String> getLabelPhrases(String label, List<String> list_phrases)
	{
		List<String> acceptable_Phrases = new ArrayList<>();
		for(String str: list_phrases)
		{
			if(label.toLowerCase().trim().contains(str.toLowerCase().trim()))
			{
				if(!acceptable_Phrases.contains(str.toLowerCase().trim()))
				{
					System.out.println("MATCH FOR " + str + " and " + label + " is ::" + str);
					acceptable_Phrases.add(str.toLowerCase().trim());
				}					
			}
			else if(Similarity_Utils.getSimilarityScore(str, label) > 0.25){
					String match = Similarity_Utils.doStringsfuzzyContainedIn(str, label);
					if(!match.trim().equals(""))
					{
						acceptable_Phrases.add(match.trim());
						System.out.println("MATCH FOR " + str + " and " + label + " is ::" + match);						
					}
			}				
		}
		
		return acceptable_Phrases;
	}
	

}
