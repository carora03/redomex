package lu.svv.chetan.models;

import java.util.Arrays;
import java.util.List;

import net.didion.jwnl.JWNLException;

public class Utilities {
	
	static WordnetHelper wn = new WordnetHelper("jwnl_prop.xml");
	
	public static String[] Tokenize(String str)
	{
		String[] result = {};
		if(str.trim().isEmpty())
			return result;
		
		result = str.split("\\s");
		return result;
	}
	
	public static boolean containsStopwords(String str)
	{
		String[] tokens = Tokenize(str.toLowerCase());
		String[] unWantedWords = {"a", "an", "and", "or"};
		for(String unwantedWord: unWantedWords)
		{			
			if(Arrays.asList(tokens).contains(unwantedWord))
			{
				return true;
			}
		}
		return false;
	}
	
	public static boolean isSingleMeaningful(String str) throws JWNLException
	{
		String[] tokens = Tokenize(str.toLowerCase());
		if(tokens.length == 1)
		{
			return wn.wordExists(str);
		}
		return false;
	}

	public static void printList(List<String> list)
	{
		for(String str: list)
		{
			System.out.println(str);
		}
	}
	
}
