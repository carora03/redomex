package lu.svv.chetan.models;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class ReadCSV {
	
	static String csvSplitBy = "(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
	public static StringJoiner sj_reqsText = new StringJoiner(System.lineSeparator());
	public static void readReqsTextFile(String fileName) {
		
		  FileReader file = null;
		  BufferedReader reader = null;
		  try {
		    file = new FileReader(fileName);
			reader = new BufferedReader(file);
		    String line = "";
		    int header = 0;
		    while ((line = reader.readLine()) != null) {
		    	if(header == 0)
		    	{
		    		header++;
		    		continue;
		    	}
		    	String [] relationsLine = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");		    	
		    	Data.map_reqIDs_Text.put(relationsLine[0].trim(), relationsLine[1].replaceAll("\"", "").trim());
		    	sj_reqsText.add(relationsLine[1].replaceAll("\"", "").trim());
		    }
		  } catch (Exception e) {
			  System.out.println("File(s) not Found" + System.lineSeparator());
			  
		      throw new RuntimeException(e);
		  } finally {
		    if (file != null) {
		      try {
		    	  file.close();
		      } catch (IOException e) {
		        // Ignore issues during closing 
		      }
		    }
		  }
		}
	
	public static void readRelationsFile(String fileName) {
		
		  FileReader file = null;
		  BufferedReader reader = null;		  
		  try {
		    file = new FileReader(fileName);
			reader = new BufferedReader(file);
		    String line = "";
		    int header = 0;
		    while ((line = reader.readLine()) != null) {
		    	if(header == 0)
		    	{
		    		header++;
		    		continue;
		    	}
		    	String [] relationsLine = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
		    	String req_Id = relationsLine[0].trim();
		    	
		    	
		    	Relation relation = new Relation();
		    	relation.setReq_Id(req_Id);
		    	relation.setRelation_Id(relationsLine[1].replaceAll("\"", "").trim());
		    	
		    	/*
		    	 * Source Related Factors
		    	 */
		    	String src = relationsLine[2].replaceAll("\"", "").trim();
		    	int src_length = Utilities.Tokenize(src).length;
		    	relation.setSource_Concept(src);
		    	relation.setSource_Concept_TC(src_length);
		    	relation.setisSourceSingleMeaningful(Utilities.isSingleMeaningful(src));
		    	relation.setisSourceHavingConjunction(Utilities.containsStopwords(src));
		    	
		    	/*
		    	 * Target Related Factors
		    	 */
		    	String target = relationsLine[3].replaceAll("\"", "").trim();
		    	String [] target_tokens = Utilities.Tokenize(target);
		    	int target_length = target_tokens.length;		  
		    	relation.setTarget_Concept(target);
		    	relation.setTarget_Concept_TC(target_length);
		    	relation.setisTargetSingleMeaningful(Utilities.isSingleMeaningful(target));
		    	relation.setisTargetHavingConjunction(Utilities.containsStopwords(target));
		    	
		    	
		    	relation.setRelation_Text(relationsLine[4].replaceAll("\"", "").trim());
		    	int relation_Length = Utilities.Tokenize(relationsLine[4].replaceAll("\"", "").trim()).length;
		    	relation.setRelation_Text_TC(relation_Length);
		    	
		    	int req_Length = Utilities.Tokenize(Data.map_reqIDs_Text.get(req_Id)).length;
		    	double rel_Req_Ratio = (src_length + target_length + relation_Length) * 1.0 / req_Length;
		    	relation.setRel_Req_Ratio(rel_Req_Ratio);
		    	
		    	relation.setType(RelationType.valueOf(relationsLine[5].replaceAll("\"", "").trim().toUpperCase()));
		    	relation.setLP_Depth(Integer.parseInt(relationsLine[6].replaceAll("\"", "").trim()));
		    	relation.setLP_Type(LinkPathType.valueOf(relationsLine[7].replaceAll("\"", "").trim().toUpperCase()));
		    	relation.setRule_Id(relationsLine[8].replaceAll("\"", "").trim());
		    	relation.setIsCorrect(CorrectnessType.valueOf(relationsLine[9].replaceAll("\"", "").trim()));
		    	relation.setIsRelevant(RelevanceType.valueOf(relationsLine[10].replaceAll("\"", "").trim()));
		    	
		    	Data.map_AllRelations.put(relation.getRelation_Id(), relation);
		    	
		    	if(Data.map_reqIDs_Relations.containsKey(req_Id))
		    	{
		    		List<Relation> list_Relation = Data.map_reqIDs_Relations.get(req_Id);
		    		list_Relation.add(relation);
		    		Data.map_reqIDs_Relations.put(req_Id,list_Relation);
		    	}
		    	else
		    	{
		    		List<Relation> list_Relation = new ArrayList<>();
		    		list_Relation.add(relation);
		    		Data.map_reqIDs_Relations.put(req_Id,list_Relation);		    		
		    	}
		    	
		    }
		  } catch (Exception e) {
			  System.out.println("File(s) not Found" + System.lineSeparator());
			  
		      throw new RuntimeException(e);
		  } finally {
		    if (file != null) {
		      try {
		    	  file.close();
		      } catch (IOException e) {
		        // Ignore issues during closing 
		      }
		    }
		  }
		}
	
	

}
