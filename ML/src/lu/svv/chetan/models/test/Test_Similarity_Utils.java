package lu.svv.chetan.models.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import lu.svv.chetan.models.RelationType;
import lu.svv.chetan.models.Similarity_Utils;

public class Test_Similarity_Utils {

	@Test
	public void test() {
		//fail("Not yet implemented");
		assertEquals(Similarity_Utils.getSimilarityScore("abc", "abc"), 1.0F);
		assertNotSame(Similarity_Utils.getSimilarityScore("abc", "abcd"), 1.0F);
		
		RelationType type = RelationType.valueOf("ASSOCIATION");
		System.out.println(type.name());
		
		List<String> list1 = new ArrayList<>();
		list1.add("abc");
		list1.add("cde");
		list1.add("abcd");
		
		List<String> list2 = new ArrayList<>();
		list2.add("abc");
		list2.add("cdef");
		list2.add("abcd");
		list2.add("abcdefgh");
		System.out.println(Similarity_Utils.is_lst1_a_Subsetof_lst2(list1, list2));
		
		System.out.println(Similarity_Utils.getSimilarityScore("cde", "cdef"));
		
		List<String> list3 = new ArrayList<>(list1);
		list3.add(0, list1.get(0) + "aaa"); 
		
		Random random = new Random(1);
		System.out.println(random.nextInt());
	}

}
