package lu.svv.chetan.models.test;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

public class HelloWorldApp {

    public static void main(String[] args) throws RserveException, REXPMismatchException, FileNotFoundException, IOException {
        RConnection c = new RConnection();
        if(c.isConnected()) {
            System.out.println("Connected to RServe.");
            if(c.needLogin()) {
                System.out.println("Providing Login");
                c.login("username", "password");
            }

            REXP x;
            System.out.println("Reading script...");
            c.eval("source(\"/Users/chetan.arora/Dropbox/PhD Folder/PhD_Papers/ESEC2017_RCode/R_CA_wd/JavaInterface.R\")");
            RList rl = c.eval("rf_results").asList();
            System.out.println(rl.size());

        } else {
            System.out.println("Rserve could not connect");
        }

        c.close();
        System.out.println("Session Closed");
    }

}