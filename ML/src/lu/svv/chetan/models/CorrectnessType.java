package lu.svv.chetan.models;

public enum CorrectnessType {
	YES("YES"), NO("NO"), PC("PC");
	
	private final String name;       

	 private CorrectnessType(String s) {
	        name = s;
}
	 
	 public boolean equalsName(String otherName) {
	        return (otherName == null) ? false : name.equals(otherName);
	    }

	    public String toString() {
	       return this.name;
	    }
}
