package lu.svv.chetan.models.searchData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import gate.Corpus;
import lu.svv.chetan.models.Data;
import lu.svv.chetan.models.DependentRules_Utils;
import lu.svv.chetan.models.GATE_NLP;
import lu.svv.chetan.models.ReadCSV;
import lu.svv.chetan.models.Relation;

public class PrepareDataforSearchAlgos_Relations {
	
	static LinkedHashMap<String, Set<String>> map_relationIDs_AllNPs = new LinkedHashMap<>();
	public static void main(String [] args)
	{
		prepareSearchData();
	}
	
	public static void prepareSearchData()
	{
		File directory = new File("");
		System.out.println(directory.getAbsolutePath());
		
		/*
		 * Read Requirements Text
		 */
			String Requirements_FileName = "SES_Data_Requirements";
			ReadCSV.readReqsTextFile("./res/req_docs/" + Requirements_FileName + ".csv");
			
			/*
			 * Process Requirements Text
			 * This fills out the NPs in the Requirements 
			 */
				try {
					Corpus corpus = GATE_NLP.init();
					System.out.println(corpus.getDocumentName(0));
					GATE_NLP.extractData(corpus);
					System.out.println(Data.map_reqIDs_NPs.size());
					System.out.println(Data.map_reqIDs_VBs.size());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String Relations_FileName = "SES_Interview_Data_learning";			
				ReadCSV.readRelationsFile("./res/relation_docs/" + Relations_FileName + ".csv");
				int count = 0;
				for(String reqID: Data.map_reqIDs_Relations.keySet())
				{
					List<Relation> list_Relations = Data.map_reqIDs_Relations.get(reqID);
					List<String> list_VBs = Data.map_reqIDs_VBs.get(reqID);
					List<String> list_NPs = Data.map_reqIDs_NPs.get(reqID);
					
					for(Relation rel: list_Relations)
					{
						count ++;
						if(!rel.getRelation_Text().trim().equals(""))
						{							
							Set<String> lst_NPs = new HashSet<String>(DependentRules_Utils.getLabelPhrases(rel.getRelation_Text().trim(), list_NPs));
							lst_NPs.add(rel.getSource_Concept());
							lst_NPs.add(rel.getTarget_Concept());
							if(lst_NPs.size() > 0)
								map_relationIDs_AllNPs.put(rel.getRelation_Id(), lst_NPs);
						}
						else{
							Set<String> lst_NPs = new HashSet<String>();
							lst_NPs.add(rel.getSource_Concept());
							lst_NPs.add(rel.getTarget_Concept());
							map_relationIDs_AllNPs.put(rel.getRelation_Id(), lst_NPs);
						}
					}
					
				}
				System.out.println(count + " relations traversed");
				System.out.println(map_relationIDs_AllNPs.size() + " relations added");
				try {
			         FileOutputStream fileOut =
			         new FileOutputStream("./res/write_docs/SES_Relation_NPs.ser");
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeObject(map_relationIDs_AllNPs);
			         out.close();
			         fileOut.close();
			         System.out.printf("Serialized data is saved in ./res/write_docs/ folder");
			      }catch(IOException i) {
			         i.printStackTrace();
			      }				
				
	}

}
