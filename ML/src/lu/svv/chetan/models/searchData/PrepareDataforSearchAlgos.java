package lu.svv.chetan.models.searchData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import gate.Corpus;
import lu.svv.chetan.models.Data;
import lu.svv.chetan.models.GATE_NLP;
import lu.svv.chetan.models.ReadCSV;

public class PrepareDataforSearchAlgos {
	
	public static void main(String [] args)
	{
		prepareSearchData();
	}
	
	public static void prepareSearchData()
	{
		File directory = new File("");
		System.out.println(directory.getAbsolutePath());
		
		/*
		 * Read Requirements Text
		 */
			String Requirements_FileName = "OpenCoss_Data_Requirements";
			ReadCSV.readReqsTextFile("./res/req_docs/" + Requirements_FileName + ".csv");
			
			/*
			 * Process Requirements Text
			 * This fills out the NPs in the Requirements 
			 */
				try {
					Corpus corpus = GATE_NLP.init();
					System.out.println(corpus.getDocumentName(0));
					GATE_NLP.extractData(corpus);
					System.out.println(Data.map_reqIDs_NPs.size());
					System.out.println(Data.map_reqIDs_VBs.size());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String Relations_FileName = "OpenCoss_Interview_Data_learning";			
				ReadCSV.readRelationsFile("./res/relation_docs/" + Relations_FileName + ".csv");
				LinkedHashMap<String, Set<String>> map = new LinkedHashMap<>();
				for(String key: Data.map_reqIDs_Relations.keySet())
				{
					map.put(key,  new HashSet<String>(Data.map_reqIDs_NPs.get(key)));
				}
				System.out.println(map.size());
				try {
			         FileOutputStream fileOut =
			         new FileOutputStream("./res/write_docs/OpenCoss_Req_NPs.ser");
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeObject(map);
			         out.close();
			         fileOut.close();
			         System.out.printf("Serialized data is saved in ./res/write_docs/ folder");
			      }catch(IOException i) {
			         i.printStackTrace();
			      }				
				
	}

}
