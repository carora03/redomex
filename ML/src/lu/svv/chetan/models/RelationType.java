package lu.svv.chetan.models;

public enum RelationType {
	ASSOCIATION("Association"),
	AGGREGATION("Aggregation"),
	GENERALIZATION("Generalization"),
	ATTRIBUTE("Attribute");
	
	 private final String name;       

	 private RelationType(String s) {
	        name = s;
}
	 
	 public boolean equalsName(String otherName) {
	        return (otherName == null) ? false : name.equals(otherName);
	    }

	    public String toString() {
	       return this.name;
	    }
	    
}
