package lu.svv.chetan.models;

import java.io.File;
import java.util.List;

import gate.Corpus;
import lu.svv.chetan.models.prepareData.RandomizeOrder;

public class MainClass {
	
	public static void main(String [] args)
	{	
		File directory = new File("");
		System.out.println(directory.getAbsolutePath());
		
		/*
		 * Read Requirements Text
		 */
			String Requirements_FileName = "SES_Data_Requirements";
			ReadCSV.readReqsTextFile("./res/req_docs/" + Requirements_FileName + ".csv");
			
			/*
			 * Process Requirements Text 
			 */
				try {
					Corpus corpus = GATE_NLP.init();
					System.out.println(corpus.getDocumentName(0));
					GATE_NLP.extractData(corpus);
					System.out.println(Data.map_reqIDs_NPs.size());
					System.out.println(Data.map_reqIDs_VBs.size());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				//executeDataCollection(1);
				for(int i = 1; i<=100; i++)
				{
					executeDataCollection(i);
				}
	}

	private static void executeDataCollection(int i)
	{
		/*
		 * Read Relations Text
		 */
			String Relations_FileName = "SES_Interview_Data_learning";			
			ReadCSV.readRelationsFile("./res/relation_docs/" + Relations_FileName + ".csv");
					
			/*
			 * Get Phrases for the association relations labels
			 */
			for(String reqID: Data.map_reqIDs_Relations.keySet())
			{
				List<Relation> list_Relations = Data.map_reqIDs_Relations.get(reqID);
				List<String> list_VBs = Data.map_reqIDs_VBs.get(reqID);
				List<String> list_NPs = Data.map_reqIDs_NPs.get(reqID);
				for(Relation rel: list_Relations)
				{
					if(!rel.getRelation_Text().trim().equals(""))
					{
						List<String> lst_VBs = DependentRules_Utils.getLabelPhrases(rel.getRelation_Text().trim(), list_VBs);
						List<String> lst_NPs = DependentRules_Utils.getLabelPhrases(rel.getRelation_Text().trim(), list_NPs);
						if(lst_VBs.size() > 0)
							Data.map_relationIDs_VBs.put(rel.getRelation_Id(), lst_VBs);
						if(lst_NPs.size() > 0)
						Data.map_relationIDs_NPs.put(rel.getRelation_Id(), lst_NPs);
					}
				}				 
			}
					
				//Randomize Order
				//RandomizeOrder.randomizeOrder_byRelations();
				//RandomizeOrder.randomizeOrder_byReqs();
				//RandomizeOrder.optimalOrder_byReqs("./res/ordered_sets/SES/SES_VAR" + i + ".txt");
				RandomizeOrder.optimalOrder_byRelations("./res/ordered_sets/SES/ordered_newObjective/SES_VAR" + i + ".txt");
				//RandomizeOrder.diversifiedOrder_byRelations();
			
				//FillDependentRulesData_TrainingSet.FillDependentRulesData();
				FillDependentRulesData.FillDependentRulesData_TrainingData();
				FillDependentRulesData.FillDependentRulesData_TestData();
				
				WriteData.writeCSV(Data.map_TrainingRelations, Relations_FileName + "_TrainingSet_" + i + ".csv");
				WriteData.writeCSV(Data.map_TestRelations, Relations_FileName + "_TestSet_" + i + ".csv");
				System.out.println(i + " ***Data Written***");
				Data.reset();
			}
	}
