library(Rserve)
#library(RSclient)
library(randomForest)
library(data.table)
library(kernlab)
library(caret)
setwd("/Users/chetan.arora/Dropbox/PhD Folder/work/Experimentation/Model_Extraction_ML/")
#Rserve(args = "--vanilla")
finalResults <- data.frame(case = factor(),
                           round = integer(),
                           algo = factor(),
                           Accuracy = double(),
                           TP = integer(),
                           FP = integer(),
                           TN = integer(),
                           FN = integer(),
                           Recall = double(),
                           Specificity = double)
levels(finalResults$case) <- c("SES")
levels(finalResults$algo) <- c("RF")

#Set Thresholds
thresholds <- read.csv("Thresholds.csv", header = FALSE)
YES_margin <- thresholds[1,1]
NO_margin <- thresholds[2,1]

iterations <- read.csv("iteration.csv", header = FALSE)
iteration_1 <- iterations[1,1]
iteration_2 <- iterations[2,1]

set.seed(7)
for(case in levels(finalResults$case))
{
  ##READ DATA##
  filename <- read.csv("filename.csv", header = FALSE)
  trainFile <- as.character(filename[1,1])
  testFile <- as.character(filename[2,1])
  mydata <- read.csv(trainFile)
  testdata <- read.csv(testFile)
  
  ##Manipulate Data to bring it in required form
  #mydata$Req_Id <- as.character(mydata$Req_Id)
  mydata$maxEndConceptTokens <- apply(mydata[,c("Source_TC", "Target_TC")], 1, function(x) max(x))
  mydata$minEndConceptTokens <- apply(mydata[,c("Source_TC", "Target_TC")], 1, function(x) min(x))
  mydata$singleMeaningfulConcepts <- apply(mydata[,c("Source_Meaningful", "Target_Meaningful")], 1, function(x) sum(x))
  mydata$ConceptswithConjunction <- apply(mydata[,c("Source_Conj", "Target_Conj")], 1, function(x) sum(x))
  mydata$Source_R_Target_R_MAX <- apply(mydata[,c("Source_R", "Target_R")], 1, function(x) max(x))
  mydata$Source_R_Target_R_MIN <- apply(mydata[,c("Source_R", "Target_R")], 1, function(x) min(x))
  mydata$Source_IR_Target_IR_MAX <- apply(mydata[,c("Source_IR", "Target_IR")], 1, function(x) max(x))
  mydata$Source_IR_Target_IR_MIN <- apply(mydata[,c("Source_IR", "Target_IR")], 1, function(x) min(x))
  
  ##SET Test Data
  #testdata$Req_Id <- as.character(testdata$Req_Id)
  testdata$maxEndConceptTokens <- apply(testdata[,c("Source_TC", "Target_TC")], 1, function(x) max(x))
  testdata$minEndConceptTokens <- apply(testdata[,c("Source_TC", "Target_TC")], 1, function(x) min(x))
  testdata$singleMeaningfulConcepts <- apply(testdata[,c("Source_Meaningful", "Target_Meaningful")], 1, function(x) sum(x))
  testdata$ConceptswithConjunction <- apply(testdata[,c("Source_Conj", "Target_Conj")], 1, function(x) sum(x))
  testdata$Source_R_Target_R_MAX <- apply(testdata[,c("Source_R", "Target_R")], 1, function(x) max(x))
  testdata$Source_R_Target_R_MIN <- apply(testdata[,c("Source_R", "Target_R")], 1, function(x) min(x))
  testdata$Source_IR_Target_IR_MAX <- apply(testdata[,c("Source_IR", "Target_IR")], 1, function(x) max(x))
  testdata$Source_IR_Target_IR_MIN <- apply(testdata[,c("Source_IR", "Target_IR")], 1, function(x) min(x))
  
  finalTrainingData_1 <- mydata[,c("Relevance", "Relation_Type", "LP.Depth", "LP.Type", "Rule_Id", "Relation_TC", "Rel_Req_Ratio",
                                   "Subset_R", "Superset_R", "CommonEnds_R", "CommonVerbs_R",
                                   "Relations_SameReq", "Relations_SameReq_R", "Relations_SameReq_SameRule_R",
                                   "maxEndConceptTokens", "minEndConceptTokens", "singleMeaningfulConcepts",
                                   "ConceptswithConjunction", "Source_R_Target_R_MAX", "Source_R_Target_R_MIN")]
  
  
  finalTrainingData_2 <- mydata[,c("Relevance", "Relation_Type", "LP.Depth", "LP.Type", "Rule_Id", "Relation_TC", "Rel_Req_Ratio",
                                   "Subset_IR", "Superset_IR", "CommonEnds_IR",
                                   "CommonVerbs_IR", "Relations_SameReq", "Relations_SameReq_IR",
                                   "Relations_SameReq_SameRule_IR", "maxEndConceptTokens", "minEndConceptTokens", "singleMeaningfulConcepts",
                                   "ConceptswithConjunction", "Source_IR_Target_IR_MAX", "Source_IR_Target_IR_MIN")]
  
  finalTestData_1 <- testdata[,c("Relevance", "Relation_Type", "LP.Depth", "LP.Type", "Rule_Id", "Relation_TC", "Rel_Req_Ratio",
                                 "Subset_R", "Superset_R", "CommonEnds_R", "CommonVerbs_R",
                                 "Relations_SameReq", "Relations_SameReq_R", "Relations_SameReq_SameRule_R",
                                 "maxEndConceptTokens", "minEndConceptTokens", "singleMeaningfulConcepts",
                                 "ConceptswithConjunction", "Source_R_Target_R_MAX", "Source_R_Target_R_MIN")]
  
  finalTestData_2 <- testdata[,c("Relevance", "Relation_Type", "LP.Depth", "LP.Type", "Rule_Id", "Relation_TC", "Rel_Req_Ratio",
                                 "Subset_IR", "Superset_IR", "CommonEnds_IR",
                                 "CommonVerbs_IR", "Relations_SameReq", "Relations_SameReq_IR",
                                 "Relations_SameReq_SameRule_IR", "maxEndConceptTokens", "minEndConceptTokens", "singleMeaningfulConcepts",
                                 "ConceptswithConjunction", "Source_IR_Target_IR_MAX", "Source_IR_Target_IR_MIN")]
  
  
  # 
  # finalTestData_1 <- testdata[,c("Relevance", "Relation_Type", "LP.Depth", "LP.Type", "Rule_Id",
  #                              "Subset_R", "CommonEnds_R", "CommonVerbs_R",
  #                              "CommonVerbs_IR", "Relations_SameReq_R", "Relations_SameReq_IR", "Relations_SameReq_SameRule_R",
  #                              "Relations_SameReq_SameRule_IR", "minEndConceptTokens",
  #                              "Source_R_Target_R_MAX", "Source_R_Target_R_MIN","Source_IR_Target_IR_MAX", "Source_IR_Target_IR_MIN")]
  
  allData_1 <- rbind(finalTrainingData_1, finalTestData_1)
  allData_2 <- rbind(finalTrainingData_2, finalTestData_2)
  
  ####Call Random FOREST
  
  learn_model_1 <- randomForest(Relevance~., 
                                data=allData_1[1:dim(finalTrainingData_1)[1],])
  learn_model_2 <- randomForest(Relevance~., 
                                data=allData_2[1:dim(finalTrainingData_2)[1],])
  
  predictions_1 <- predict(learn_model_1, allData_1[(dim(finalTrainingData_1)[1]+1):(dim(allData_1)[1]),], type='prob')
  predictions_2 <- predict(learn_model_2, allData_2[(dim(finalTrainingData_2)[1]+1):(dim(allData_2)[1]),], type='prob')
  
  YES_threshold <- ((1 - min(predictions_1[,"NO"])) - YES_margin)
  NO_threshold <- max(predictions_1[,"NO"]) - NO_margin
  
  rf_results_1 <- data.frame(nrow=1:NROW(finalTestData_1))
  rf_results_1$RelId <- testdata$Rel_Id
  rf_results_1$predicted <- allData_1[(dim(finalTrainingData_1)[1]+1):(dim(allData_1)[1]), "Relevance"]
  levels(rf_results_1$predicted) <- c("NO", "YES", "UD")
  rf_results_1$predicted[predictions_1[,"YES"] < YES_threshold & predictions_1[,"NO"] < NO_threshold] <- "UD"
  rf_results_1$predicted[predictions_1[,"YES"] >= YES_threshold] <- "YES"
  rf_results_1$predicted[predictions_1[,"NO"] >= NO_threshold] <- "NO"
  
  YES_threshold <- ((1 - min(predictions_2[,"NO"])) - YES_margin)
  NO_threshold <- max(predictions_2[,"NO"]) - NO_margin
  
  rf_results_2 <- data.frame(nrow=1:NROW(finalTestData_2))
  rf_results_2$RelId <- testdata$Rel_Id
  rf_results_2$predicted <- allData_2[(dim(finalTrainingData_2)[1]+1):(dim(allData_2)[1]), "Relevance"]
  levels(rf_results_2$predicted) <- c("NO", "YES", "UD")
  rf_results_2$predicted[predictions_2[,"YES"] < YES_threshold & predictions_2[,"NO"] < NO_threshold] <- "UD"
  rf_results_2$predicted[predictions_2[,"YES"] >= YES_threshold] <- "YES"
  rf_results_2$predicted[predictions_2[,"NO"] >= NO_threshold] <- "NO"
  
  
  rf_results_1$Relevance <- allData_1[(dim(finalTrainingData_1)[1]+1):(dim(allData_1)[1]), "Relevance"]
  rf_results_1$finalPrediction <- as.factor(ifelse(rf_results_1$predicted == rf_results_2$predicted, as.character(rf_results_1$predicted), "UD"))
  #rf_results$predicted_2 <- rf_results_2$predicted
} 
rf_results <- rf_results_1
# rf_results_1$NO <- predictions_1[,"NO"]

# plot.new()
# fl <- paste("Plot", iteration_1, iteration_2,".pdf", sep = "_")
# #write.csv(rf_results_1, fl)
# pdf(fl)
# plot(x=rf_results_1$NO, y=rf_results_1$RelId, type="n")
# text(x=rf_results_1$NO, y=rf_results_1$RelId, labels=paste(rf_results_1$RelId, rf_results_1$Relevance))
# dev.off()
rf_results

