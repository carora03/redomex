# README #

### What is this repository for? ###

* This repository contains code for extracting domain model from NL requirements.

### How do I get set up? ###

* The repository can be cloned and the Model_Extraction_Main.java class can be executed as the main class to run the code on a set of requirements.